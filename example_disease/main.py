sim_path = '/home/ngeard/projects/repos/simodd-pop-pub'
import os, sys, time
sys.path.append(sim_path)
print sys.path

import numpy as np
import tables as tb

from random import Random
from population.utils import parse_params, merge_params, create_path

from disease.general.contact_matrix import ContactMatrix
from disease.SEIR.disease_SEIR import DiseaseSEIR
from disease.observers.obs_disease import DiseaseObserver
from disease.observers.obs_cases_table import CaseObserver

from disease.experiments.disease_experiments import go_single, output_single 


class DiseaseModel(DiseaseSEIR):
    """
    Local version of SIR disease, adding observers and vaccines specific 
    to this set of experiments.
    """

    def __init__(self, p, cmatrix, rng, fname, mode='w'):
        super(DiseaseModel, self).__init__(p, cmatrix, rng, fname, mode)

        self.add_observers(
            CaseObserver(self.h5file, p['t_per_year'], entry_state='E'),
            DiseaseObserver(self.h5file, self.state_labels(), 
                self.disease_states),
            )    # observers track various statistics during sim


# - # - # - # - # - # - # - # - # - # - # - # - # - # - # - # - # - # - # - #
# - # - MAIN  - # - #
# - # - # - # - # - # - # - # - # - # - # - # - # - # - # - # - # - # - # - #

def load_params():
    print "Loading parameters..."
    pop_params = parse_params('params_pop.cfg', 
            os.path.join(sim_path, 'population/paramspec_pop.cfg'))
    epi_params = parse_params('params_SEIR.cfg', 
            os.path.join(sim_path, 'disease/SEIR/paramspec_sir.cfg'))
    p = merge_params(pop_params, epi_params)
    return p


def show_usage():
    print "Usage:"
    print "  test s/o : run/output a single simulation"
    exit()


if __name__ == '__main__':

    ### setup parameters
    p = load_params()
    p['pop_prefix'] = p['prefix']
    p['epi_prefix'] = p['prefix']
    create_path(p['prefix'])
    print p

    # simulation specific parameters
    p['cov_steps'] = [1]
    p['cov_levels'] = 0.9
    
    p['overwrite'] = p['overwrite_cp'] = True# NB: always overwrite in example script
    
    # run simulation
    go_single(p, DiseaseModel, ContactMatrix(), p['seed'], verbose=True)

    # load output file and generate plots
    disease = DiseaseModel(p, ContactMatrix(), Random(p['seed']),
            os.path.join(p['prefix'], 'disease.hd5'), mode='r')
    output_single(p, disease)
            







