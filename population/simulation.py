"""
simulation.py

Agent-based simulation a population with pseudo-realistic demography.
"""

import os
from random import Random
from math import exp

from population.pop_hh import PopHH
from population.individual import Individual
from population.utils import sample_table, load_probs, \
    load_probs_new, load_age_rates, load_prob_tables, load_prob_list


def adjust_prob(P, t_per_year):
    """
    convert from an annual rate to a per-time-period probabiliy
    where time-period = 1/t_per_year
    """
    N = 1.0 / t_per_year
    return 1 - pow(1 - P, N)


class Simulation(object):
    """
    Basic demographic simulation object.

    Handles updating of population age and household structure.
    """

    def __init__(self, p, ind_type=Individual, create_pop=True):
        # convert ConfigObj to dictionary to store params
        self.p = dict(p)
        self.p_adj = {}
        self.rng = Random(self.p['seed'])

        self.growth_residue = 0.0
        # for storing general data
        self.max_hh = 50  # maximum possible hh size
        self.pop_size = []
        self.age_dist = []
        self.hh_size_dist = []
        self.hh_size_dist_counts = []
        self.hh_size_avg = []
        self.hh_count = []
        self.fam_types = []
        self.start_time = None
        self.end_time = None

        self.hh_comp = None
        self.death_rates = None
        self.fertility_age_probs = None
        self.fertility_parity_probs = None
        self.dyn_years = None

        self.setup_params()

        self.P = None
        if create_pop:
            self.create_population(ind_type, p['logging'])

    def setup_params(self):
        """
        Reset various simulation parameters.

        (could be cleaner what is going on here between initialization
        and resetting...)
        """
        self.growth_residue = 0.0
        # for storing general data
        self.max_hh = 50  # maximum possible hh size
        self.pop_size = []
        self.age_dist = []
        self.hh_size_dist = []
        self.hh_size_dist_counts = []
        self.hh_size_avg = []
        self.hh_count = []
        self.fam_types = []
        self.start_time = None
        self.end_time = None
        self.load_demographic_data()

    def create_population(self, ind_type, logging=True):
        """
        Create a population according to specified age and household size
        distributions.
        """
        self.setup_params()
        self.P = PopHH(ind_type, logging)
        self.P.gen_hh_age_structured_pop(
            self.p['pop_size'], self.hh_comp,
            self.age_dist, self.p['age_cutoffs'], self.rng)
        self.P.allocate_couples()

    @staticmethod
    def parse_age_rates(filename, factor, final):
        """
        Parse an age-year-rate table to produce a dictionary, keyed by age, 
        with each entry being a list of annual rates (by year).
        
        Setting final to 'True' appends an age 100 rate of >1 (e.g., to 
        ensure everyone dies!
        """

        dat = load_age_rates(filename)
        rates = {}
        for line in dat:
            rates[line[0]] = [x * factor for x in line[1:]]
        if final:
            rates[101] = [100 for _ in dat[0][1:]]  # everybody dies...
        return rates

    def load_demographic_data(self):
        """
        Load data on age-specific demographic processes (mortality/fertility)
        and adjust event probabilities according to time-step.
        """

        # load household size distribution and age distribution
        self.hh_comp = load_probs(os.path.join(self.p['resource_prefix'],
                                               self.p['hh_composition']), False)
        self.p['age_cutoffs'] = [int(x) for x in self.hh_comp[0][1:][0]]  # yuk!
        self.age_dist = load_probs(os.path.join(self.p['resource_prefix'],
                                                self.p['age_distribution']))

        annual_factor = 1.0 / self.p['t_per_year']

        # load and scale MORTALITY rates
        self.death_rates = {
            0: self.parse_age_rates(os.path.join(
                self.p['resource_prefix'],
                self.p['death_rates_m']), annual_factor, True),
            1: self.parse_age_rates(os.path.join(
                self.p['resource_prefix'],
                self.p['death_rates_f']), annual_factor, True)}

        ### load FERTILITY age probs (don't require scaling) for closed pops
        self.fertility_age_probs = load_prob_tables(os.path.join(
            self.p['resource_prefix'],
            self.p['fertility_age_probs']))
        self.fertility_parity_probs = load_probs_new(os.path.join(
            self.p['resource_prefix'],
            self.p['fertility_parity_probs']))

        ### load and scale leave/couple/divorce and growth rates
        if self.p['dyn_rates']:
            # rates will be a list of annual values
            self.p['leaving_probs'] = load_prob_list(os.path.join(
                self.p['resource_prefix'], self.p['leaving_prob_file']))
            self.p['couple_probs'] = load_prob_list(os.path.join(
                self.p['resource_prefix'], self.p['couple_prob_file']))
            self.p['divorce_probs'] = load_prob_list(os.path.join(
                self.p['resource_prefix'], self.p['divorce_prob_file']))
            self.p['growth_rates'] = load_prob_list(os.path.join(
                self.p['resource_prefix'], self.p['growth_rate_file']))
            self.p['imm_rates'] = load_prob_list(os.path.join(
                self.p['resource_prefix'], self.p['imm_rate_file']))

            self.p_adj['leaving_probs'] = [adjust_prob(x, self.p['t_per_year'])
                                           for x in self.p['leaving_probs']]
            self.p_adj['couple_probs'] = [adjust_prob(x, self.p['t_per_year'])
                                          for x in self.p['couple_probs']]
            self.p_adj['divorce_probs'] = [adjust_prob(x, self.p['t_per_year'])
                                           for x in self.p['divorce_probs']]
            self.p_adj['growth_rates'] = [adjust_prob(x, self.p['t_per_year'])
                                          for x in self.p['growth_rates']]
            self.p_adj['imm_rates'] = [adjust_prob(x, self.p['t_per_year'])
                                       for x in self.p['imm_rates']]

            self.dyn_years = min(len(self.death_rates[0][0]) - 1,
                                 len(self.fertility_age_probs) - 1,
                                 len(self.p_adj['leaving_probs']) - 1,
                                 len(self.p_adj['couple_probs']) - 1,
                                 len(self.p_adj['divorce_probs']) - 1,
                                 len(self.p_adj['growth_rates']) - 1)

        else:
            # adjust demographic event probabilities according to time step
            self.p_adj['couple_probs'] = [adjust_prob(
                self.p['couple_prob'], self.p['t_per_year'])]
            self.p_adj['leaving_probs'] = [adjust_prob(
                self.p['leaving_prob'], self.p['t_per_year'])]
            self.p_adj['divorce_probs'] = [adjust_prob(
                self.p['divorce_prob'], self.p['t_per_year'])]
            self.p_adj['growth_rates'] = [adjust_prob(
                self.p['growth_rate'], self.p['t_per_year'])]
            self.p_adj['imm_rates'] = [adjust_prob(
                self.p['imm_rate'], self.p['t_per_year'])]

    def update_individual_demo(self, t, ind, index=0):
        """
        Update individual ind; check for death, couple formation, leaving home
        or divorce, as possible and appropriate.
        """

        death = None
        birth = None

        couple_prob = self.p_adj['couple_probs'][index]
        #        if ind.divorced and ind.deps: couple_prob *= 0.5

        # DEATH / BIRTH:
        if self.rng.random() > exp(-self.death_rates[ind.sex][ind.age][index]):
            death = ind
            mother = ind
            # make sure the dead individual isn't selected as mother!
            while mother is ind:
                mother = self.choose_mother(index)
            if mother == "error":
                return "error", "error"
            birth = self.update_death_birth(t, ind, mother)

        # COUPLE FORMATION:
        elif self.p['couple_age'] < ind.age < 60 \
                and not ind.partner \
                and self.rng.random() < couple_prob:
            partner = self.choose_partner(ind)
            if partner:
                self.P.form_couple(t, ind, partner)

        # LEAVING HOME:
        elif ind.age > self.p['leaving_age'] \
                and ind.with_parents \
                and not ind.partner \
                and self.rng.random() < self.p_adj['leaving_probs'][index]:
            self.P.leave_home(t, ind)

        # DIVORCE:
        elif self.p['divorce_age'] < ind.age < 50 \
                and ind.partner \
                and self.rng.random() < self.p_adj['divorce_probs'][index]:
            self.P.separate_couple(t, ind)

        # ELSE: individual has a quiet year...
        return death, birth

    def choose_mother(self, index):
        """
        Choose a new mother on the basis of fertility rates.

        NOTE: there is still a very small possibility (in *very* small
        populations) that this will hang due to candidates remaining
        forever empty.  Should add a check to prevent this and exit gracefully.
        """

        candidates = []
        attempts = 0
        max_attempts = 500  # before restarting with a new population
        while not candidates:
            tgt_age = int(sample_table(
                self.fertility_age_probs[index], self.rng)[0])
            tgt_prev_min = 0
            tgt_prev_max = 100
            if self.p['use_parity']:
                tgt_prev_min = int(sample_table(
                    self.fertility_parity_probs[(tgt_age-15)/5], self.rng)[0])
                # effectively transform 5 into 5+
                tgt_prev_max = tgt_prev_min if tgt_prev_min < 5 else 20
            tgt_set = self.P.individuals_by_age(tgt_age, tgt_age)
            #print len(tgt_set)
            #print [(x.sex, x.can_birth(), not x.with_parents, len(x.children)) for x in tgt_set]
            candidates = [
                x for x in tgt_set
                if x.sex == 1 and x.can_birth() and not x.with_parents
                and tgt_prev_min <= len(x.children) <= tgt_prev_max
            ]
            #print candidates
            attempts += 1
            if attempts >= max_attempts:
                # probably a better way to do this, but this "error" is
                # currently propagated upwards (eventually to sim_epi)
                return "error"
        return self.rng.choice(candidates)

    def choose_partner(self, ind):
        """
        Choose a partner for i_id, subject to parameter constraints.

        :param ind: the first partner in the couple.
        :type ind: ind_type
        :returns: partner if successful, otherwise None.
        """

        mean_age = ind.age + self.p['partner_age_diff'] \
            if ind.sex == 0 else ind.age - self.p['partner_age_diff']
        tgt_age = 0
        candidates = []
        while tgt_age < self.p['min_partner_age']:
            tgt_age = int(self.rng.gauss(mean_age, self.p['partner_age_sd']))
            tgt_set = self.P.individuals_by_age(tgt_age, tgt_age)
            candidates = [
                x for x in tgt_set
                if not x.partner and x.sex != ind.sex
                and x not in self.P.hh_members(ind)
            ]

        # abort if no eligible partner exists
        return None if candidates == [] else self.rng.choice(candidates)

    def update_death_birth(self, t, ind, mother):
        """
        Replace a dying individual with a newborn.  If no individual to die
        is passed, only a birth occurs; if no mother is passed, only a death
        occurs.

        :param t: the current time step.
        :type t: int
        :param ind: the individual to die.
        :type ind: ind_type
        :returns: the new individual.
        """

        if ind:
            orphans = self.P.death(t, ind)
            self.P.process_orphans(t, orphans, 18, self.rng)

        if mother:
            sex = self.rng.randint(0, 1)
            new_ind = self.P.birth(t, self.rng, mother, mother.partner, sex)
            return new_ind

        return None

    def update_all_demo(self, cur_t):
        """
        Update population one time step (of duration 364/t_per_year days)

        Returns list of births, deaths, immigrants and birthdays.
        """

        # age each individual by appropriate number of days
        birthdays = self.P.age_population(364 / self.p['t_per_year'])

        deaths = []
        births = []

        # calculate index for fertility and mortality rates
        # basically: use first entry for burn-in, then one entry every 
        # 'period' years, then use the final entry for any remaining years.
        index = min(max(
            0, (cur_t - (self.p['demo_burn'] * self.p['t_per_year'])) /
               (self.p['demo_period'] * self.p['t_per_year'])), self.dyn_years) \
            if self.p['dyn_rates'] else 0

        #print cur_t / self.p['t_per_year'], \
        #     index, self.p_adj['growth_rates'][index], \
        #     len(self.P.I)

        cur_inds = self.P.I.values()
        for ind in cur_inds:
            death, birth = self.update_individual_demo(cur_t, ind, index)
            if death == "error" and birth == "error":
                return "error", "error", "error", "error"
            if death:
                deaths.append(death)
            if birth:
                births.append(birth)

        #population growth
        # calculate number of new individuals to add (whole and fraction)
        new_individuals = len(self.P.I) * self.p_adj['growth_rates'][index]
        # get whole part of new individuals
        new_now = int(new_individuals)
        # add fractional part to residue accumulation
        self.growth_residue += (new_individuals - new_now)
        # grab any new 'whole' individuals
        new_residue = int(self.growth_residue)
        new_now += new_residue
        self.growth_residue -= new_residue

        # create the new individuals
        for _ in xrange(new_now):
            mother = self.choose_mother(index)
            if mother == "error":
                return "error", "error", "error", "error"
            births.append(self.update_death_birth(cur_t, None, mother))

        #immigration
        imm_count = 0
        imm_tgt = int(len(self.P.I) * self.p_adj['imm_rates'][index])
        source_hh_ids = []
        immigrants = []
        while imm_count < imm_tgt:
            hh_id = self.rng.choice(self.P.groups['household'].keys())
            imm_count += len(self.P.groups['household'][hh_id])
            source_hh_ids.append(hh_id)
        for hh_id in source_hh_ids:
            new_hh_id = self.P.duplicate_household(cur_t, hh_id)
            immigrants.extend(self.P.groups['household'][new_hh_id])

        return births, deaths, immigrants, birthdays

    def record_stats_demo(self, t):
        """
        Keep track of various demographic statistics.
        """

        #TODO: this should be transformed into the observer framework

        if (self.p['record_interval'] <= 0) \
                or (t % self.p['record_interval'] is not 0):
            return

        self.pop_size.append(len(self.P.I))
        self.age_dist.append(self.P.age_dist(101, 101)[0])
        self.hh_size_dist.append(
            self.P.group_size_dist('household', self.max_hh)[0])
        self.hh_size_dist_counts.append(
            self.P.group_size_dist('household', self.max_hh, False)[0])
        self.hh_size_avg.append(self.P.group_size_avg('household'))
        self.hh_count.append(len(self.P.groups['household']))

        self.fam_types.append(self.P.sum_hh_stats_group())
