"""
A stub for testing/evaluating dynamic demography simulations.
"""
import sys, time
sim_path = '/home/ngeard/projects/repos/simodd-pop-pub'
sys.path.append(sim_path)
print sys.path

import cPickle as pickle
from random import Random

from population.utils import parse_params, create_path
from population.output_pop import *
from population.simulation import Simulation


def load_params():
    print os.path.dirname(__file__)
    p = parse_params('params.cfg', os.path.join(sim_path,
                                                'population/paramspec_pop.cfg'))
    create_path(p['prefix'])
    print p
    return p


def run_single(p):
    print "Creating population..."
    sim = Simulation(p)
    timesteps = p['years'] * p['t_per_year']
    print "Running simulation..."
    sim.start_time = time.time()
    for i in xrange(timesteps):
        t = i * 364 / p['t_per_year']
        b, d, im, bd = sim.update_all_demo(i)
        print i, t, len(sim.P.I), len(b), len(d), len(im)
        sim.record_stats_demo(t)
    sim.end_time = time.time()
    return sim


def save_sim(sim, fname):
    print "Saving simulation..."
    sim.P.flatten()
    pickle.dump(sim, open(fname, 'wb'), pickle.HIGHEST_PROTOCOL)
    sim.P.lift()


def load_sim(fname):
    print "Loading simulation..."
    sim = pickle.load(open(fname, 'rb'))
    sim.P.lift()
    return sim


def go_single(p):
    if p['random_seed']:
        p['seed'] = Random().randint(0, 99999999)
    fname = os.path.join(p['prefix'], 'final_%d.sim' % p['seed'])
    if os.path.isfile(fname):
        sim = load_sim(fname)
    else:
        sim = run_single(p)
        save_sim(sim, fname)
    return sim


def output_single(sim):
    print "Processing data and generating output plots..."

    fig_prefix = sim.p['prefix']
    create_path(fig_prefix)

    output_hh_life_cycle(sim, os.path.join(fig_prefix, 'hh_life_cycle.%s' % sim.p['ext']))
    output_comp_by_hh_size(sim, os.path.join(fig_prefix, 'age_by_hh_size.%s' % sim.p['ext']))
    output_comp_by_hh_age(sim, os.path.join(fig_prefix, 'age_by_hh_age.%s' % sim.p['ext']))
    output_hh_size_distribution(sim, 10, os.path.join(fig_prefix, 'hh_size_dist.%s' % sim.p['ext']))
    output_age_distribution(sim, os.path.join(fig_prefix, 'age_dist.%s' % sim.p['ext']))
    output_household_type(sim, os.path.join(fig_prefix, 'hh_type.%s' % sim.p['ext']))
    output_household_composition(sim, os.path.join(fig_prefix, 'hh_comp.%s' % sim.p['ext']))
    output_hh_size_time(sim, os.path.join(fig_prefix, 'hh_size_time.%s' % sim.p['ext']), comp=False)
    output_fam_type_time(sim, os.path.join(fig_prefix, 'fam_type_time.%s' % sim.p['ext']), comp=False)

    print 'Output written to ', sim.p['prefix']


if __name__ == '__main__':
    p = load_params()
    p['ext'] = 'svg'
    sim = go_single(p)
    output_single(sim)


