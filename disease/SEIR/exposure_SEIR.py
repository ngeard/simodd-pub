from math import sin, pi

from disease.general.exposure_base import Exposure


class ExposureSEIR(Exposure):
    def __init__(self, p):
        super(ExposureSEIR, self).__init__(p)
        self.phi = p['phi']
        self.iphi = 1.0 - p['phi']
        self.q = 0
        self.init_q(p)
        self.two_pi = 2 * pi / p['t_per_year']
        self.sf_amp = p['sf_amp']

    def init_q(self, p):
        self.q = p['q'] * 364 / p['t_per_year']

    def calc_foi_fast(self, t, ind, P, comm_precomp, rng):
        """
        Calculate the probability that an individual is exposed to infection.
        """
        # calculate weighted household contribution to effective contact rate
        hh_infected = [x for x in P.housemates(ind) if x.state.infectious]

        # get NUMBER of infected individuals in ind's household
        hh = self.phi * len(hh_infected)

        # calculate weighted community contribution from precomputed values
        comm = self.iphi * comm_precomp['I'][ind.age]

        # compute combined effective contact rate
        combined = hh + comm

        ind.hh_frac = hh / combined if combined > 0.0 else 0.0

        #if combined > 0.0:
        #    ind.hh_source = rng.random() < ind.hh_frac
        #
        #if ind.hh_source:
        #    ind.source = rng.sample(hh_infected, 1)[0]

        ind.hh_source = 0
        ind.source = 0

        # compute seasonal forcing term
        sf = (1.0 + (self.sf_amp * sin(t * self.two_pi))) if self.sf_amp > 0 else 1.0

        return self.q * sf * combined
