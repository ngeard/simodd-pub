from disease.general.disease_base import DiseaseBase
from disease.general.duration import DurationGenerator
from disease.SEIR.exposure_SEIR import ExposureSEIR
from disease.SEIR.states_SEIR import *


# Class to do either SEIR or SIR outbreak
class DiseaseSEIR(DiseaseBase):
    def __init__(self, p, cmatrix, rng, fname, mode):
        super(DiseaseSEIR, self).__init__(p, None, cmatrix, fname, mode)
        self.exposure = ExposureSEIR(p)
        rem = Removed(3)
        inf = Infected(2, DurationGenerator(
            p['infective_mean'] / (364.0 / p['t_per_year']), p['k'], rng))

        if p['exposed_mean'] > 0:
            exp_ = Exposed(1, DurationGenerator(
                p['exposed_mean'] / (364.0 / p['t_per_year']), p['k'], rng))
            sus = Susceptible(0)
            self.add_states(sus, exp_, inf, rem)
            self.disease_states = ['I', 'E']
        else:
            sus = Susceptible(0)
            self.add_states(sus, inf, rem)


class DiseaseSIR(DiseaseBase):
    def __init__(self, p, cmatrix, rng, fname, mode):
        super(DiseaseSIR, self).__init__(p, None, cmatrix, fname, mode)
        self.exposure = ExposureSEIR(p)
        rem = Removed(3)
        inf = Infected(2, DurationGenerator(
            p['infective_mean'] / (364.0 / p['t_per_year']), p['k'], rng))
        sus = Susceptible(0)
        self.add_states(sus, inf, rem)


class DiseaseSIRS(DiseaseBase):
    def __init__(self, p, cmatrix, rng, fname, mode):
        super(DiseaseSIRS, self).__init__(p, None, cmatrix, fname, mode)
        self.exposure = ExposureSEIR(p)
        rem = RemovedTemp(3, DurationGenerator(
            p['removed_mean'] / (364.0 / p['t_per_year']), p['k'], rng))
        inf = Infected(2, DurationGenerator(
            p['infective_mean'] / (364.0 / p['t_per_year']), p['k'], rng))
        sus = Susceptible(0)
        self.add_states(sus, inf, rem)


