"""
Setup for multiple runs (to aggregate stats over)
"""

import socket
hostname = socket.gethostname()
print 'hostname:', hostname
if hostname.startswith('bruce'):
    sim_path = '/vlsci/VR0274/ngeard/repos/simodd'
    resource_path = '/vlsci/VR0274/ngeard/data/demographic/'
else:
    sim_path = '/home/ngeard/projects/repos/simodd'
    resource_path = '/home/ngeard/projects/demography/data/'
import os, sys, time
sys.path.append(sim_path)

from random import Random
from itertools import product

import numpy as np
import tables as tb

from population.utils import parse_params, merge_params, create_path

from disease.general.contact_matrix import ContactMatrix
from disease.SEIR.disease_SEIR import DiseaseSIR, DiseaseSIRS
from disease.experiments.disease_experiments import *
from disease.experiments.param_sweeps import *

from disease.observers.obs_disease import DiseaseObserver
from disease.observers.obs_cases_table import CaseObserver
from disease.observers.obs_hh_status import *
from disease.observers.obs_vaccine import VaccineObserver
from disease.observers.obs_hh_at_risk import HHAtRiskObserver
from disease.observers.obs_hh_susceptible import HHSusceptibleObserver
from disease.observers.obs_import import ImportsObserver
from disease.observers.obs_outbreak import OutbreakObserver
from disease.observers.obs_contact_matrix import ContactMatrixObserver


class DiseaseSIRS_local(DiseaseSIRS):

    def __init__(self, p, cmatrix, rng, fname, mode='w'):
        super(DiseaseSIRS_local, self).__init__(p, cmatrix, rng, fname, mode)
        self.add_observers(
                #ContactMatrixObserver(self.h5file,
                #    age_classes = cmatrix.age_classes,
                #    t_interval_basic = p['t_per_year']*20,
                #    t_interval_derived = p['t_per_year']*20,
                #    sample_size=1000)
                
                #CaseObserver(self.h5file, p['t_per_year']),
                DiseaseObserver(self.h5file, self.state_labels(), 
                    self.disease_states),
                #HHStatusObserver(self.h5file, 'hh_immunity', 
                #    always_true, not_at_risk, p['t_per_year']/4),
                #HHSusceptibleObserver(self.h5file)
                )


class DiseaseSIR_local(DiseaseSIR):

    def __init__(self, p, cmatrix, rng, fname, mode='w'):
        super(DiseaseSIR_local, self).__init__(p, cmatrix, rng, fname, mode)
        self.add_observers(
                #ContactMatrixObserver(self.h5file,
                #    age_classes = cmatrix.age_classes,
                #    t_interval_basic = p['t_per_year']*20,
                #    t_interval_derived = p['t_per_year']*20,
                #    sample_size=1000)
                
                CaseObserver(self.h5file, p['t_per_year']),
                DiseaseObserver(self.h5file, self.state_labels(), 
                    self.disease_states),
                #HHStatusObserver(self.h5file, 'hh_immunity', 
                #    always_true, not_at_risk, p['t_per_year']/4),
                #HHSusceptibleObserver(self.h5file)
                )


# - # - # - # - # - # - # - # - # - # - # - # - # - # - # - # - # - # - # - #
# - # - MAIN  - # - #
# - # - # - # - # - # - # - # - # - # - # - # - # - # - # - # - # - # - # - #


def load_params(pop_type, dis_type):
    print "Loading parameters..."
    pop_params = parse_params('params_pop_%s.cfg' % pop_type, 
            os.path.join(sim_path, 'population/paramspec_pop.cfg'))
    epi_params = parse_params('params_%s.cfg' % dis_type, 
            os.path.join(sim_path, 'disease/SEIR/paramspec_sir.cfg'))
    p = merge_params(pop_params, epi_params)
    return p


def show_usage():
    print "Usage:"
    print "  test s/o : run/output a single simulation"
    print "  test sw/o : run/output a parameter sweep"
    exit()


if __name__ == '__main__':

    ### setup parameters
    pop_types = ['stable', 'dynamic']
    dis_types = ['SIR', 'SIRS']

    p = {}
    for pop_type, dis_type in product(pop_types, dis_types):
        p_cur = load_params(pop_type, 'SIRS') # NB: also works for SIR
        base_prefix = p_cur['prefix']
        label = '%s-%s' % (pop_type, dis_type)
        p_cur['prefix'] = os.path.join(p_cur['prefix'], 
                '%s-%s' % (pop_type, dis_type))
#        p_cur['pop_prefix'] = base_prefix
#        p_cur['epi_prefix'] = p_cur['prefix']
        p_cur['resource_prefix'] = resource_path
        create_path(p_cur['prefix'])
        p[label] = p_cur

    # scenario specific parameters

    # SIR - stable

    p['stable-SIR']['pop_size'] = 50000
    p['stable-SIR']['years'] = [0, 100] 
    p['stable-SIR']['epi_burn_in'] = 100

    # SIR - dynamic

    p['dynamic-SIR']['pop_size'] = 100
    p['dynamic-SIR']['years'] = [0, 100] 
    p['dynamic-SIR']['epi_burn_in'] = 100
    p['dynamic-SIR']['cm_update_years'] = 10
    p['dynamic-SIR']['demo_burn'] = p['dynamic-SIR']['burn_in'] \
            + p['dynamic-SIR']['epi_burn_in']

    # SIRS - stable

    p['stable-SIRS']['pop_size'] = 50000
    p['stable-SIRS']['years'] = [0, 100] 
    p['stable-SIRS']['epi_burn_in'] = 100

    # SIRS - dynamic

    p['dynamic-SIRS']['pop_size'] = 100
    p['dynamic-SIRS']['years'] = [0, 100] 
    p['dynamic-SIRS']['epi_burn_in'] = 100
    p['dynamic-SIRS']['cm_update_years'] = 10
    p['dynamic-SIRS']['demo_burn'] = p['dynamic-SIR']['burn_in'] \
            + p['dynamic-SIR']['epi_burn_in']

    # overwrite existing output files if 'x' switch is passed
    for k in p:
        p[k]['overwrite'] = ('x' in sys.argv)
        p[k]['overwrite_cp'] = ('x' in sys.argv)

    # suspect there has to be at least one sweep param...
    sweep_params = [
            {'name': 'q', 'values': [0.05, 0.1]}
            ]

    pop_types = ['dynamic']
    dis_types = ['SIR']

    if len(sys.argv) < 2:
        show_usage()

    elif sys.argv[1] == 's':
        for pop_type, dis_type in product(pop_types, dis_types):
            label = '%s-%s' % (pop_type, dis_type)
            if dis_type == 'SIR':
                dis_class = DiseaseSIR_local
            elif dis_type == 'SIRS':
                dis_class = DiseaseSIRS_local
            go_single(p[label], dis_class, 
                    ContactMatrix(), p[label]['seed'], verbose=True)

    elif sys.argv[1] == 'so':
        for pop_type, dis_type in product(pop_types, dis_types):
            label = '%s-%s' % (pop_type, dis_type)
            if dis_type == 'SIR':
                dis_class = DiseaseSIR_local
            elif dis_type == 'SIRS':
                dis_class = DiseaseSIRS_local
            d = dis_class(p[label], ContactMatrix(),
                Random(p[label]['seed']), 
                    os.path.join(p[label]['prefix'], 'disease.hd5'), mode='r')
            output_single(p[label], d)

    elif sys.argv[1] == 'sw':
        for pop_type, dis_type in product(pop_types, dis_types):
            label = '%s-%s' % (pop_type, dis_type)
            if dis_type == 'SIR':
                dis_class = DiseaseSIR_local
            elif dis_type == 'SIRS':
                dis_class = DiseaseSIRS_local
            go_sweep_mpi(p[label], sweep_params, dis_class, ContactMatrix(),
                    process_diseases_parallel)

    elif sys.argv[1] == 'swo':
        for pop_type, dis_type in product(pop_types, dis_types):
            label = '%s-%s' % (pop_type, dis_type)
            if dis_type == 'SIR':
                dis_class = DiseaseSIR_local
            elif dis_type == 'SIRS':
                dis_class = DiseaseSIRS_local
            go_sweep_mpi(p[label], sweep_params, dis_class, ContactMatrix(),
                    output_diseases_parallel)

    elif sys.argv[1] == 'swag':
        for pop_type, dis_type in product(pop_types, dis_types):
            label = '%s-%s' % (pop_type, dis_type)

            p[label]['num_runs'] = 2

            if dis_type == 'SIR':
                dis_class = DiseaseSIR_local
            elif dis_type == 'SIRS':
                dis_class = DiseaseSIRS_local
            sr, output = go_sweep_mpi(p[label], sweep_params, dis_class, 
                    ContactMatrix(), go_output_aggregate)

            print "Output!"
 
            agg_output = aggregate_over_seeds(output, p[label]['num_runs'])

            print agg_output[0].keys()

            param_combo_it = ParamComboIt(
                    p[label], sweep_params, use_seeds=False)

            param_combo_it.print_combos()

            times = get_times(p[label])

            for cur_combo in param_combo_it:
                output_timeseries(
                        os.path.join(cur_combo['p']['prefix'], 'agg_hh_frac.png'),
                        times, agg_output[0]['mean_hh_frac'], 'Age',
                        y_errs=agg_output[0]['sd_hh_frac'])

                output_timeseries(
                        os.path.join(cur_combo['p']['prefix'], 'agg_age_first_infection.png'),
                        times, agg_output[0]['mean_avg_age_first_infection'], 'Age',
                        y_errs=agg_output[0]['sd_avg_age_first_infection'])

                print len(agg_output[0]['mean_inc_by_hh_size_ss_1'])
                plot_statistics(
                        os.path.join(cur_combo['p']['prefix'], 'agg_inc_by_hh_size.png'),
                        range(len(agg_output[0]['mean_inc_by_hh_size_ss_1'])), 
                        [agg_output[0]['mean_inc_by_hh_size_ss_%d'%x] for x in range(5)], 
                        'Household size', 'Incidence',
                        #series_labels=['t=%d'%(x/p[label]['t_per_year']) for x in ....
                        y_errs=[agg_output[0]['sd_inc_by_hh_size_ss_%d'%x] for x in range(5)])

    
    else:
        show_usage()

    






