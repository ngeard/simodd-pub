import numpy as np


class DurationGenerator(object):
    """
    Generates random durations for 'time in compartment' (e.g., infectious, exposed)
    according to a Gamma distribution with specified mean and shape parameter k (essentially
    equivalent to the sum of k exponential distributions)
    """

    def __init__(self, mean, k, rng):
        self.k = k
        self.theta = float(mean) / k
        self.rng = np.random.RandomState(rng.randint(0, 99999999))

    def get_duration(self):
        return int(round(self.rng.gamma(self.k, self.theta)))
