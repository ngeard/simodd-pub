"""
Template for updating disease state in a population.
"""
import tables as tb
from itertools import chain

import numpy as np


class DiseaseBase(object):
    def __init__(self, p, exposure, cmatrix, fname, mode,
                 basic_susceptible='S', basic_infection='I',
                 disease_states=('I')):
        self.halt = p['halt']
        self.external_exposure_rate = p['external_exposure_rate']
        self.exposure = exposure
        self.basic_susceptible = basic_susceptible
        self.basic_infection = basic_infection
        self.birth_condition = self.basic_susceptible
        self.disease_states = disease_states
        self.cmatrix = cmatrix
        self.cnetwork = None
        self.obs_on = True
        self.logging = p['logging']
        self.birth_count = 0
        self.death_count = 0
        self.v_factor = 364 / p['t_per_year']

        self.states = {}
        self.infectious_states = None

        # for tracking number of different types of infected individuals per age class
        self.by_age = {}

        self.observers = {}
        self.vaccines = []

        self.h5file = tb.openFile(fname, mode)
        if mode in 'w':
            self.store_params(p)

    def store_params(self, p):
        if '/params' not in self.h5file:
            self.h5file.createGroup('/', 'params', 'Parameters')
        for k, v in p.items():
            self.h5file.setNodeAttr('/params', k, v)
        self.h5file.setNodeAttr('/params', 'complete', False)
        self.h5file.flush()

    def is_complete(self):
        return self.h5file.getNodeAttr('/params', 'complete')

    def done(self, complete=False):
        if complete:
            self.h5file.setNodeAttr('/params', 'complete', True)
        self.h5file.close()

    ### Initialisation and clean-up # # # # # # # # # # # #

    def set_counts(self, P):
        """
        Initialise state counts and I_by_age on basis of state of 
        individuals in population P.
        """
        # reset counts to zero
        for state in self.states.values():
            state.count = 0
        for label in self.infectious_states:
            self.by_age[label] = [0] * len(self.cmatrix.age_classes)

        # set counts on basis of P
        for ind in P.I.values():
            # make sure individual is using this disease's state objects
            if ind.prev_state:
                ind.prev_state = self.states[ind.prev_state.label]
            ind.state = self.states[ind.state.label]
            if ind.next_state:
                ind.next_state = self.states[ind.next_state.label]
            # update state count
            self.states[ind.state.label].count += 1
            # update I_by_age dict

            if ind.state.label in self.infectious_states:
                self.by_age[ind.state.label][self.cmatrix.age_map[ind.age]] += 1
                ind.state.current.add(ind.ID)

        # update age-sorted lists of individuals
        self.cmatrix.init_age_classes(P)

    def add_states(self, *states):
        """Add a new disease state."""
        for state in states:
            self.states[state.label] = state
        self.add_infectious_states()

    def add_infectious_states(self):
        """Add infectious states."""
        self.infectious_states = [label for label in self.disease_states if self.states[label].infectious]
        for label in self.infectious_states:
            self.by_age[label] = [0] * len(self.cmatrix.age_classes)

    def add_observers(self, *observers):
        """Add a new observer to the observers list."""
        for observer in observers:
            self.observers[observer.label] = observer

    def seed_infection(self, t, P, cases, rng, seed_inds=None):
        """Seed initial infection (set everyone else to susceptible)."""
        for ind in P.I.values():
            ind.next_state = self.states[self.basic_susceptible]
            self.tick(t, ind)
        if not seed_inds:
            seed_inds = rng.sample(P.I.values(), cases)
        for ind in seed_inds:
            ind.next_state = self.states[self.basic_infection]
            ind.source = -3
            I_in_age, I_out_age = self.tick(t, ind)
            if I_in_age >= 0:
                self.by_age[self.basic_infection][self.cmatrix.age_map[I_in_age]] += 1

    ### Updating  # # # # # # # # # # # # # # # # # # # # #

    def bd_update(self, t, births, deaths, imms):
        """
        Update state counts for births and deaths and immigration.
        
        Currently immigrants are treated as arriving susceptible.
        
        TODO: update this to handle, e.g., transfer of maternal immunity.
        """
        for ind in chain(births, imms):
            ind.next_state = self.states['S']
            self.tick(t, ind)
        for ind in deaths:
            if ind.state:
                ind.state.exit(t, ind)
                if ind.state.label in self.infectious_states:
                    self.by_age[ind.state.label][self.cmatrix.age_map[ind.age_at_infection]] -= 1
            ind.state = None
        self.birth_count = len(births)
        self.death_count = len(deaths)

    def update_observers(self, t, **kwargs):
        # store observed data (if observers are switched on)
        if self.obs_on:
            for observer in self.observers.values():
                observer.update(t, **kwargs)

    def update(self, t, P, rng):
        """Update the disease state of the population."""

        self.check_vaccines(t, P)
        cases = self.check_exposure(t, P, rng)
        introduction = self.external_exposure(t, P, rng)
        new_I = self.update_ind_states(t, P)

        self.update_observers(t, disease=self, pop=P,
                              cases=cases['infection'],
                              boosting=cases['boosting'],
                              introduction=introduction,
                              new_I=new_I, rng=rng)

        # if halting, halt if no exposed or infected individuals in population (eg for SIR)
        return self.halt and \
               sum([self.states[x].count for x in self.disease_states]) == 0

    def check_vaccines(self, t, P):
        # reset vaccine counts and evaluate vaccinations
        for v in self.vaccines:
            v.count = 0
            # generate a pool of candidates who are of the target age for the vaccine
            candidates = [x for x in P.individuals_by_age(v.age) if
                          x.age_days / self.v_factor == v.age_days / self.v_factor]
            for ind in candidates:
                if v.check_coverage(t, ind):
                    ind.next_state = self.states[v.v_state]

    def check_exposure(self, t, P, rng):

        # we will return a list of exposures (broken into infectious cases and boosting)
        cases = dict(infection=[], boosting=[])

        # return empty lists if there is currently no force of infection acting
        if sum([self.states[label].count for label in self.infectious_states]) == 0:
            return cases

        # create a set of individuals currently at risk
        # split depending upon whether network or matrix is being used to calculate community exposure
        pop_at_risk = set()
        comm = {}
        if self.cnetwork:
            # population at risk consists of household members and network neighbours
            for cur_I in self.states[self.basic_infection].current:
                pop_at_risk.update([x for x in P.housemates(P.I[cur_I]) if x.state.at_risk])
                pop_at_risk.update([x for x in self.cnetwork.get_contacts(P, P.I[cur_I]) if x.state.at_risk])
        else:
            # population at risk consists of potentially everybody
            pop_at_risk = [x for x in P.I.values() if x.state.at_risk]
            # compute exposure from community:
            # comm is the force of infection arising from infection in the community
            # EC[i] is a vector containing the contact rates between an individual in age group i
            # and individuals in each age group j, weighted by the (approximate) number of people
            # in age group j (as community mixing is density dependent).
            # I_by_age is the number of infected individuals in each age group j
            # total force of infection for each age class i is equal to the products of
            # weighted contact rate and the number of infected individuals, summed over
            # each age class i.
            for label in self.infectious_states:
                comm[label] = np.array([np.sum(self.cmatrix.EC[i] * self.by_age[label]) for i in range(101)])

        # test exposure for each individual at risk
        for ind in pop_at_risk:
            # split depending upon whether network or matrix is being used to calculate community exposure
            if self.cnetwork:
                foi = self.exposure.calc_foi_fast(t, ind, P, self.cnetwork, rng)
            else:
                foi = self.exposure.calc_foi_fast(t, ind, P, comm, rng)
            exposure_type = ind.state.test_exposure(self.states, ind, foi, rng)
            #                    p, s = P.hh_parents_siblings(ind)
            #                    p_I = len([x for x in p if x.state.infectious])
            #                    s_I = len([x for x in s if x.state.infectious])
            if exposure_type == 'infection':
                ind.infections.append(t)
                cases['infection'].append(ind)
            elif exposure_type == 'boosting':
                cases['boosting'].append(ind)
        return cases

    def external_exposure(self, t, P, rng):
        """Check for external import of infection."""
        external_exposure_prob = \
            self.external_exposure_rate * len(P.I)
        if external_exposure_prob < 0:
            return False
        if rng.random() > external_exposure_prob:
            return False
        tries = 0
        max_tries = 100
        while tries < max_tries:
            ind = rng.sample(P.I.values(), 1)[0]
            if ind.state.label == self.basic_susceptible \
                    or ind.state.label == 'Se':  # ugly hack for SIRW with naive/exp compartments
                ind.next_state = self.states[self.basic_infection]
                ind.source = -2
                return ind
            tries += 1
        return False

    def update_ind_states(self, t, P):
        # returns a list of newly infectious ('symptomatic' individuals)
        new_I = []
        # second loop updates current state
        for ind in P.I.values():
            old_state = ind.state.label
            I_in_age, I_out_age = self.tick(t, ind)
            new_state = ind.state.label

            if old_state not in self.infectious_states and new_state in self.infectious_states:
                new_I.append(ind)
            if I_in_age >= 0 and new_state in self.infectious_states:
                self.by_age[new_state][self.cmatrix.age_map[I_in_age]] += 1
            if I_out_age >= 0 and old_state in self.infectious_states:
                self.by_age[old_state][self.cmatrix.age_map[I_out_age]] -= 1

        return new_I

    def tick(self, t, ind):
        """
        Update the state a single individual.
        
        Used to be a class of individual, but wanted to be able to pass in self.states
        (so that state.successors could contain labels rather than references.)
        """
        recovered_age = None
        infected_age = None
        if ind.state != ind.next_state:
            if ind.state and ind.state.infectious:
                recovered_age = ind.age_at_infection
                ind.age_at_infection = None
            if ind.next_state and ind.next_state.infectious:
                infected_age = ind.age
                ind.age_at_infection = ind.age
            if self.logging:
                ind.add_log(t, ind.next_state.label,
                            'Individual entering state: %s' % ind.next_state.label)
            if ind.state:
                ind.state.exit(t, ind)
            ind.next_state.enter(t, ind)
        ind.state.update(t, ind, self.states)
        return infected_age, recovered_age

    ### Information  # # # # # # # # # # # # # # # # # # # # #

    def state_labels(self):
        """Return a list of state labels, in specified order."""
        return [v.label for v in sorted(
            self.states.values(), key=lambda x: x.order)]

    def state_colors(self):
        """Return a list of state labels, in specified order."""
        return [v.color for v in sorted(
            self.states.values(), key=lambda x: x.order)]

    def state_counts(self):
        """Return a list of state counts, in specified order."""
        return [v.count for v in sorted(
            self.states.values(), key=lambda x: x.order)]