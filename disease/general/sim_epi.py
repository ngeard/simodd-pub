"""
sim.py

Simulation loop for epidemic simulations.
"""
import time

from population.pop_hh import PopHH
from population.simulation import Simulation
from population.output_pop import *
from disease.general.ind_epi import IndEpi


class SimEpi(Simulation):
    def __init__(self, p, disease, rng, ind_type=IndEpi):
        super(SimEpi, self).__init__(p, ind_type, create_pop=False)
        self.disease = disease
        self.rng = rng
        self.disease.firstborn_count = 0
        self.disease.subsequent_count = 0

    def init_contact_matrix(self, t=0):
        # initialise contact matrix
        print "Initialising contact matrix; t=%d" % t
        self.disease.cmatrix.init_age_classes(self.P)
        #self.disease.cmatrix.init_a_levels(self.P)
        if self.p['cm_gauss']:
            self.disease.cmatrix.init_contact_matrix_gaussian(
                self.p['epsilon'], self.p['sigma_2'])
        else:
            self.disease.cmatrix.init_contact_matrix(self.p['epsilon'])

    def init_contact_network(self):
        self.disease.cnetwork.create_network(self.P, self.disease.cmatrix, self.rng)

    def demo_burn_in(self):
        """
        Load burnt in population if exists.

        Run the demographic component of the simulation for specified number
        of years (typically 100) using (typically!) annual updating steps 
        in order to generate a starting population that avoids the artifactual
        traits of the bootstrap population
        """

        # if a specific location for the burnt in population isn't 
        # specified, look for (and save) it in the output directory
        if 'pop_prefix' not in self.p:
            print "WARNING!: pop_prefix not specified!"
            self.p['pop_prefix'] = self.p['prefix']

        pop_filename = os.path.join(self.p['pop_prefix'],
                                    'burn_in_%d.pop' % self.p['pop_size'])

        if os.path.isfile(pop_filename):
            print "Loading previously burnt in population"
            self.P = PopHH.load(pop_filename)
            print "Loaded (%d individuals in %d households)" % (len(self.P.I), len(self.P.groups['household']))
        else:
            print "No burn in population found in %s; creating new population..." \
                  % self.p['pop_prefix']

            timesteps = self.p['burn_in'] * (self.p['burn_in_t_per_year'])
            real_t_per_year = self.p['t_per_year']
            self.p['t_per_year'] = self.p['burn_in_t_per_year']
            self.load_demographic_data()
            success = False
            while not success:
                self.create_population(IndEpi, self.p['logging'])
                for i in xrange(timesteps):
                    #print "burn in; year", i
                    b, d, i, b2 = self.update_all_demo(i)
                    if b == "error":
                        success = False
                        break
                else:
                    success = True
            self.p['t_per_year'] = real_t_per_year
            self.load_demographic_data()
            print "Burn in complete; saving population..."
            self.P.save(pop_filename)
            print "Population saved."

    def epi_burn_in(self, start_year, verbose=True):
        """
        Load burnt in population if exists.

        Otherwise, run an epidemic for specified number of years (i.e., 100) to 
        produce a stable population states.  Save the resulting population.
        """

        # if a specific location for the burnt in population isn't 
        # specified, look for (and save) it in the output directory
        if 'epi_prefix' not in self.p:
            print "WARNING!: epi_prefix not specified!"
            self.p['epi_prefix'] = self.p['prefix']

        epi_filename = os.path.join(self.p['epi_prefix'],
                                    'burn_in_epi_%d.pop' % self.p['pop_size'])
        if os.path.isfile(epi_filename):
            print "Loading previously burnt in epidemic"
            self.P = PopHH.load(epi_filename)
            self.disease.set_counts(self.P)
        else:
            print "No burn in epidemic found in %s; creating new epidemic." \
                  % self.p['epi_prefix']
            self.demo_burn_in()
            print "Demographic burn in complete/loaded; running epidemic burn in..."

            self.init_contact_matrix()
            # seed infection, switch observers off, burn in and save population
            self.disease.seed_infection(start_year * self.p['t_per_year'], self.P,
                                        self.p['initial_cases'], self.rng)
            self.disease.obs_on = False
            self.run(self.p['burn_in'], self.p['epi_burn_in'], verbose)
            self.disease.obs_on = True
            print "Epidemic burn in complete; saving population..."
            self.P.save(epi_filename)
            print "Population saved."

    def setup(self, start_year=0, verbose=True, seed_inds=None):
        """
        Setup population for start of simulation.
        
        If no epidemic burn in is required, run (or load) demographic burn in  
        and seed infection.  Otherwise, run (or load) epidemic burn in.
        """

        if self.p['epi_burn_in'] <= 0:
            self.demo_burn_in()
            self.disease.seed_infection(start_year * self.p['t_per_year'], self.P,
                                        self.p['initial_cases'], self.rng, seed_inds)
        else:
            self.epi_burn_in(start_year, verbose)

        # if run procedure is *always* called via run_cp, then this can be removed (?)
        if self.disease.cnetwork:
            # (TODO: this is a bit ugly)
            print "Initialising contact matrix (setup)..."
            self.init_contact_matrix(start_year * self.p['t_per_year'])
            print "Initialising contact network..."
            self.init_contact_network()

    def run(self, year_begin, years, verbose=False):
        """
        Run simulation.
        """
        t_begin = int(year_begin * self.p['t_per_year'])
        t_end = int((year_begin + years) * self.p['t_per_year'])

        if verbose:
            self.start_time = time.time()
            self.print_column_labels()
            self.print_pop_numbers(t_begin)

        self.disease.update_observers(t_begin, disease=self.disease, pop=self.P,
                                      cases=[],
                                      boosting=[],
                                      introduction=True,
                                      new_I=[], rng=self.rng)

        for t in xrange(t_begin + 1, t_end + 1):
            # update demography (if required)
            if self.p['update_demog']:  # and t%52==0:
                births, deaths, imms, birthdays = self.update_all_demo(t)  # *52)
                firstborns = len([x for x in births if x.birth_order == 1])
                self.disease.firstborn_count += firstborns
                self.disease.subsequent_count += (len(births) - firstborns)
                if self.p['dyn_rates'] and t % (self.p['cm_update_years'] * self.p['t_per_year']) == 0:
                    self.init_contact_matrix(t)
                self.disease.bd_update(t, births, deaths, imms)

            # update disease
            if self.disease.update(t, self.P, self.rng):
                if verbose:
                    self.print_pop_numbers(t)
                break  # update returns true if halting upon fade out

            if verbose:
                self.print_pop_numbers(t)

        if verbose:
            self.print_column_labels()
            self.end_time = time.time()
            print "time:", self.end_time - self.start_time

    def run_cp(self, start_year, years, observers, verbose, prefix):
        """
        A version of run (above) that checks for the existence of a
        checkpoint file before running, and loads that by preference.
        Otherwise, run is called normally, and a checkpoint file is 
        saved (in directory specified by prefix).
        
        'observers' is a Boolean value specifying whether observers
        should be switched on or off for this period.
        """

        cp_filename = os.path.join(prefix, 'cp_%d-%d.pop' % (start_year, start_year + years))
        print "Current checkpoint: %s (observers=%s)" % (cp_filename, observers)
        if os.path.isfile(cp_filename) and not self.p['overwrite_cp']:
            print "Loading previous checkpoint (years %d-%d)" % (start_year, start_year + years)
            self.P = PopHH.load(cp_filename)
            self.disease.set_counts(self.P)
        else:
            print "No checkpoint found for year %d; running..." % start_year

            self.init_contact_matrix(start_year * self.p['t_per_year'])

            # switch observers off, burn in and save population
            self.disease.obs_on = observers
            self.disease.firstborn_count = 0
            self.disease.subsequent_count = 0
            self.run(start_year, years, verbose)
            if self.p['save_cp']:
                self.P.save(cp_filename)

    ### OUTPUT AND HELPER FUNCTIONS ###########################################

    def print_column_labels(self):
        print self.disease.state_labels()
        print ''.join(['%7s' % x
                       for x in
                       (['t', 't(y)', 't(d)'] + self.disease.state_labels())])

    def print_pop_numbers(self, t):
        print ''.join(['%7d' % x
                       for x in
                       ([t, t / self.p['t_per_year'],
                         t % self.p['t_per_year'] * (364.0 / self.p['t_per_year'])]
                        + self.disease.state_counts())])
