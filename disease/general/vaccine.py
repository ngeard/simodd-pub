def cov_gen_interrupt(time_points, cov_levels):
    """
    Helper function for generating heterogeneous patterns of disease coverage.
    Given, eg, time_points=[2,5,3], cov_levels=[0.9, 0.6, 0.8], the coverage pattern:
        [0.9, 0.9, 0.6, 0.6, 0.6, 0.6, 0.6, 0.8, 0.8, 0.8]
    will be generated (ie 0.9 for the first 2 years, followed by 0.6 for five years,
    then 0.8 for three years, and beyond)
    """
    cov = []
    for tp, lev in zip(time_points, cov_levels):
        cov.extend([lev] * tp)
    return cov


class VaccineBase(object):
    """
    Base vaccine class
    """

    def __init__(self, label, age, age_days, coverage, t_per_year, offset, rng, precond, fail_prob=0.0):

        self.label = label  # vaccine id

        self.age = age  # age when vaccine is given (year component)
        self.age_days = age_days  # age when vaccine is given (day component)
        self.t_per_year = t_per_year  # store locally for convenience
        self.coverage = coverage  # the coverage for each year
        self.v_state = 'R'  # the disease state that being vaccinated moves you into
        self.offset = offset  # offset due to burn in durations
        self.precond = precond  # vaccine preconditions (ie, primary course needed before booster)
        self.fail_prob = fail_prob  # probability of vaccine failure

        self.rng = rng
        self.count = 0  # number of times this vaccine has been provided

    def get_t_years(self, t):
        """
        work out where we are in the coverage schedule
        if t_years < 0, vaccination hasn't started yet
        if t_years > len(coverage) then use the final coverage value
        """
        return min(t / self.t_per_year - self.offset, len(self.coverage) - 1)

    def check_basic_conditions_met(self, t_years, ind):
        """
        Returns true if basic conditions are not met:
        -- vaccine is operating
        -- individual is of appropriate age to receive vaccine
        -- individual has not previously received vaccine
        """
        # fail if basic vaccine conditions are not met
        fail = t_years < 0 \
            or (self.label in ind.vaccines_received)

        # fail if vaccine has unmet preconditions (i.e., booster dependant on primary)
        if self.precond:
            if not set(self.precond).issubset(ind.vaccines_received):
                fail = True

        return not fail

    def check_coverage(self, t, ind, rng):
        """
        Check if individual will receive vaccine.  Must be defined by subclasses.
        """
        pass

    def give_vaccine(self, t, ind):
        """
        Administer vaccine to individual.
        """
        self.count += 1
        ind.vaccines_received.append(self.label)
        ind.vaccine_times.append(t)


class VaccineSingle(VaccineBase):
    """
    Basic single vaccination, decision made on an individual basis.
    """

    def __init__(self, label, age, age_days, coverage, t_per_year, offset, rng, precond=None, fail_prob=0.0):
        super(VaccineSingle, self).__init__(label, age, age_days, coverage, t_per_year, offset, rng, precond, fail_prob)

    def check_coverage(self, t, ind, rng):
        # if passed end of set coverage values, use final value
        t_years = self.get_t_years(t)

        if not self.check_basic_conditions_met(t_years, ind):
            return

        x = self.rng.random()
        vaccine_given = (x < self.coverage[t_years])

        # check for vaccine failure: NB: we need to record a failed vaccine as received,
        # but not actually protect the individual (i.e., so they may still receive boosters that
        # can actually be effective)
        # TODO: fix this, as currently not recorded as received!
        if vaccine_given and self.fail_prob > 0.0 and self.rng.random() < self.fail_prob:
            vaccine_given = False

        if vaccine_given:
            self.give_vaccine(t, ind)

        return vaccine_given


class VaccineHH(VaccineBase):
    """
    Vaccination on the basis of households.  Decision is made for firstborn and applies for all
    subsequent children born to that household.
    """

    def __init__(self, label, age, age_days, coverage, t_per_year, offset, rng, precond=None, fail_prob=0.0):
        super(VaccineHH, self).__init__(label, age, age_days, coverage, t_per_year, offset, rng, precond, fail_prob)

        # a dictionary mapping household ID to boolean specifying whether or not
        # it is a vaccinating household
        self.hh_vacc = {}

    def check_coverage(self, t, ind, rng):

        t_years = self.get_t_years(t)

        if not self.check_basic_conditions_met(t_years, ind):
            return

        hh_id = ind.groups['household']
        #print self.hh_vacc, hh_id

        # if this hh has made a previous vaccine decision, stick with that
        if hh_id in self.hh_vacc:
            vaccine_given = self.hh_vacc[hh_id]
        # otherwise work out whether vaccination will occur
        else:
            x = self.rng.random()
            vaccine_given = x < self.coverage[t_years]

        # check for vaccine failure: NB: we need to record a failed vaccine as received,
        # but not actually protect the individual (i.e., so they may still receive boosters that
        # can actually be effective)
        if vaccine_given and self.fail_prob > 0.0 and self.rng.random() < self.fail_prob:
            vaccine_given = False

        if vaccine_given:
            self.give_vaccine(t, ind)
            self.hh_vacc[hh_id] = vaccine_given

        return vaccine_given
