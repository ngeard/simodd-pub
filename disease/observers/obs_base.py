import tables as tb

"""
Base class for disease observers.
"""

class Observer(object):

    def __init__(self, h5file, label, description, title):
        self.label = label
        self.h5file = h5file
        if self.h5file.mode in ['w', 'a']:
            self.create_storage(description, title)
        else:
            self.load_storage()


    def create_storage(self, description, title):
        if '/%s' % self.label not in self.h5file:
            group = self.h5file.createGroup('/', self.label, title)
        filters = tb.Filters(complevel=9)   # TODO: investigate BLOSC (?)

        self.data = self.h5file.createTable(group, 'base_data', description, filters=filters)
        self.row = self.data.row
#    __create_storage = create_storage
    
    
    def load_storage(self):
        self.data = self.h5file.getNode(self.h5file.root, self.label).base_data 
        self.row = self.data.row
#    __load_storage = load_storage
    

    def update(self, t, **kwargs):
        pass

