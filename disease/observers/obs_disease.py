"""
Observer object for disease state (S, E, I, R, etc.) counts.
"""
from disease.observers.obs_base import Observer
import tables as tb
import numpy as np
import os
from disease.experiments.output_disease import output_timeseries
import matplotlib.pyplot as plt


class DiseaseObserver(Observer):
    def __init__(self, h5file, state_labels, disease_states):
        self.state_labels = ['t'] + state_labels
        self.disease_states = disease_states
        desc = dict((x, tb.UInt32Col(pos=i)) for i, x in enumerate(self.state_labels))
        super(DiseaseObserver, self).__init__(h5file, 'disease', desc, 'Disease Observer')


    def update(self, t, disease, introduction, **kwargs):
        self.row['t'] = t
        for x in self.state_labels[1:]:
            self.row[x] = disease.states[x].count
        self.row.append()
        self.h5file.flush()
#        if introduction:
#            self.introductions.append((introduction, t))


    def get_counts_by_state(self, label):
        return self.data.col(label)


    def get_props_by_state(self):
        """
        Return a dictionary mapping state labels to proportions.
        """
        sizes = np.sum([self.data.col(x) for x in self.state_labels[1:]],axis=0)
        return dict((x, self.data.col(x)/sizes.astype(float)) for x in self.state_labels[1:])


    def output_all(self, p, times):
        props = self.get_props_by_state()
        
        # plot prevalence
        output_timeseries(os.path.join(p['prefix'], 'disease_prevalence.png'),
                          times, [props[x] for x in self.disease_states], 'Prevalence', labels=self.disease_states)
    
        # plot all disease states as proportions
        output_timeseries(os.path.join(p['prefix'], 'disease_all_states.png'),
                      times, [props[x] for x in self.state_labels[1:]],
                      'Proportion', labels=self.state_labels[1:], logscale=True)
                

    
    def get_first_fadeout_time(self, state_labels):
        timeseries = sum(self.get_counts_by_state(y) for y in state_labels)
        try:
            zeros = np.where(timeseries==0)[0]
            #print len(zeros)
            if len(zeros) > 0:
                first_zero = zeros[0]
            else:
                first_zero = len(timeseries)-1
        except Exception:
            print "first zero problem!"
        time = self.data[first_zero]['t']
        return time
    
    
    def get_time_at_max(self, state_label):
        timeseries = [x for x in self.get_counts_by_state(state_label)]
        peak_inc = max(timeseries)
        index = timeseries.index(peak_inc)
        time = self.data[index]['t']
        return time
