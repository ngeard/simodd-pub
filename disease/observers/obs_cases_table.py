"""
Observer for information on case locations.
"""
from math import ceil
import tables as tb
from collections import defaultdict
from scipy.optimize import curve_fit

from pylab import NaN

from disease.observers.obs_base import Observer
from disease.experiments.output_disease import *


class Case(tb.IsDescription):
    t = tb.UInt32Col()
    t_E = tb.UInt32Col()
    ID = tb.UInt32Col()
    state = tb.StringCol(4)
    age = tb.UInt8Col()
    age_days = tb.UInt16Col()
    hh_size = tb.UInt8Col()
    hh_age = tb.UInt8Col()
    hh_type = tb.StringCol(16)
    num_siblings = tb.UInt8Col()
    num_parents = tb.UInt8Col()
    birth_order = tb.UInt8Col()
    first = tb.BoolCol()
    hh_frac = tb.Float32Col()
    hh_id = tb.UInt32Col()
    hh_source = tb.BoolCol()
    ext_contacts = tb.UInt8Col()
    ext_infections = tb.UInt8Col()
    hh_infections = tb.UInt8Col()
    source_id = tb.UInt32Col()


class CaseObserver(Observer):
    def __init__(self, h5file, t_per_year=364, entry_state='I'):
        super(CaseObserver, self).__init__(h5file, 'cases', Case, 'Case Data')
        self.t_per_year = t_per_year
        self.entry_state = entry_state

    def create_storage(self, description, title):
        """
        Called when initialised with a h5file opened in 'w'rite mode.
        Adds tables for storage of data (plus local links)
        """

        Observer.create_storage(self, description, title)
        group = self.h5file.getNode('/', 'cases')
        self.age_dists = self.h5file.createTable(group, 'age_dists',
                                                 {'t': tb.UInt32Col(), 'dist': tb.UInt32Col(shape=(101))},
                                                 'Age Distributions')
        self.age_dist = self.age_dists.row
        self.age_dists_month = self.h5file.createTable(group, 'age_dists_month',
                                                       {'t': tb.UInt32Col(), 'dist': tb.UInt32Col(shape=(20))},
                                                       'Age Distributions (months)')
        self.age_dist_month = self.age_dists_month.row
        self.hh_size_dists = self.h5file.createTable(group, 'hh_size_dists',
                                                     {'t': tb.UInt32Col(), 'dist': tb.UInt32Col(shape=(20))},
                                                     'Household Size Distributions')
        self.hh_size_dist = self.hh_size_dists.row

    def load_storage(self):
        """
        Called when initialised with a h5file opened in 'r'ead or 'a'ppend mode.
        Creates local links to existing tables.
        """

        Observer.load_storage(self)
        self.age_dist = self.h5file.root.cases.age_dists.row
        self.hh_size_dist = self.h5file.root.cases.hh_size_dists.row
        self.age_dists = self.h5file.root.cases.age_dists
        self.age_dist = self.age_dists.row
        #if self.h5file.root.cases.__contains__('age_dists_month'):
        self.age_dists_month = self.h5file.root.cases.age_dists_month
        self.age_dist_month = self.age_dists_month.row
        self.hh_size_dists = self.h5file.root.cases.hh_size_dists
        self.hh_size_dist = self.hh_size_dists.row

    ### NB: following won't work, as create_storage needs arguments.
    ### If this is actually used anywhere, will need fixing!!!
    ### 10/1/14: looks ok to me? only used in estimate_R.py
    def reset(self):
        """
        Remove all data and re-create empty storage.
        """
        self.h5file.removeNode('/', 'cases', recursive=True)
        self.create_storage(Case, 'Case Data')

    def add_new_case(self, t, disease, pop, ind):
        """
        Append details of a new case to data table.
        """

        p, s = pop.hh_parents_siblings(ind)

        self.row['t_E'] = t
        self.row['t'] = t + ind.durations.get('E', 0)
        self.row['ID'] = ind.ID
        self.row['state'] = ind.state.label
        self.row['age'] = ind.age
        self.row['age_days'] = ind.age_days
        self.row['hh_size'] = pop.hh_size(ind)
        self.row['hh_age'] = pop.hh_age(t, ind)
        self.row['hh_type'] = pop.get_hh_type(ind)
        self.row['num_siblings'] = s
        self.row['num_parents'] = p
        self.row['birth_order'] = ind.birth_order
        self.row['first'] = len(ind.infections) == 1
        self.row['hh_id'] = ind.groups['household']
        self.row['hh_frac'] = ind.hh_frac
        self.row['hh_source'] = ind.hh_source
        if disease.cnetwork:
            self.row['ext_contacts'] = disease.cnetwork.G.degree(ind.ID)
        self.row['ext_infections'] = 0
        self.row['hh_infections'] = 0
        self.row['source_id'] = ind.source

        self.row.append()
        self.data.flush()

        # update ext_infections count for source of infection
        s_id = ind.source

        #print s_id
        if not s_id or s_id < 0:
            return  # no valid source
        source_row = self.data.getWhereList('ID == s_id')
        if len(source_row) >= 1:
            if ind.hh_source:
                self.data.cols.hh_infections[source_row[-1]] += 1
            else:
                #print "incrementing external infection counter for", s_id
                self.data.cols.ext_infections[source_row[-1]] += 1

    def update(self, t, disease, pop, cases, **kwargs):
        """
        Update details of all new cases occurring this time step.
        Also (optionally) store additional population data.
        """

        # add cases
        for ind in cases:
            self.add_new_case(t, disease, pop, ind)
        # add age and household size distributions (4 times per year)
        if t % self.t_per_year in range(0, self.t_per_year, self.t_per_year / 4):
            self.age_dist['t'] = t
            self.age_dist['dist'] = pop.age_dist(norm=False)[0]
            self.age_dist.append()
            self.age_dist_month['t'] = t
            self.age_dist_month['dist'] = pop.age_dist_month(bin_days=91, max_age=5, norm=False)[0]
            self.age_dist_month.append()
            self.hh_size_dist['t'] = t
            self.hh_size_dist['dist'] = pop.group_size_dist('household', 20, norm=False)[0]
            self.hh_size_dist.append()

        # Need to flush occasionally to avoid PerformanceWarnings
        self.h5file.flush()

    #########################################################################
    ### query and analysis functions ###

    def filter_data_by_state(self):
        """
        Returns a dictionary mapping state (e.g., I, Ie, etc.) to data
        in that state.  Used if there is more than one type of infectious
        state.
        
        NB: note that values in filtered are iterators.
        """

        uniqvals = set()
        for x in self.data:
            uniqvals.add(x['state'])
        filtered = {}
        for cur_state in uniqvals:
            filtered[cur_state] = self.data.where('state == cur_state')
        return filtered

    def get_num_cases(self, st=None, et=None):
        """
        Get number of cases in specified period.
        """

        if st is None or et is None:
            return self.data.nrows
        else:
            return len([x for x in self.data.where('(t>=st) & (t<et)')])

    def get_case_IDs(self, st=None, et=None):
        """
        Get IDs of cases in specified period.
        """
        if st is None or et is None:
            return self.data.col('ID')
        else:
            return self.data.col('ID').where('(t>=st) & (t<et)')

    def get_new_data_by_time(self, dt=1):
        """
        Returns a dictionary mapping times to the number of new data occurring at that time.
        """
        case_t = defaultdict(int)
        for x in self.data:
            case_t[(x['t'] / dt)] += 1
        return case_t

    def get_new_case_counts_by_time(self, st, et, dt):
        case_t = self.get_new_data_by_time(dt)
        new_data = []
        for cur_t in xrange(st / dt, et / dt):
            new_data.append(case_t.get(cur_t, 0))
        return new_data

    def get_cum_case_count(self, st, et, t_per_year=364):
        """
        Return a list of the cumulative number of cases.
        """

        return [self.get_num_cases(st, x + 1) for x in range(st, et, (t_per_year / 364))]

    def get_first_case_time(self):
        """
        Get time of first case.
        """

        return self.data[0]['t_E']

    #########################################################################
    ## age at first infection 

    def get_age_first_infection(self, st=None, et=None):
        """
        Get list of ages of first infection in specified time period.
        """
        if st is None or et is None:
            return [x['age'] for x in self.data.where('first')]
        else:
            return [x['age'] for x in self.data.where('first & (t>=st) & (t<et)')]

    def get_age_first_infection_series(self, st=None, et=None, dt=1):
        cur_st = st
        cur_et = min(cur_st + dt, et)
        avg_age = []
        while cur_st < et:
            ages = self.get_age_first_infection(cur_st, cur_et)
            avg_age.append((np.mean(ages), np.std(ages))
                           if len(ages) > 0 else (np.nan, np.nan))
            cur_st = cur_et
            cur_et += dt
        return avg_age

    def get_hh_age_first_infection(self, st=None, et=None):
        """
        Get a list of ages of first infection in specified time period,
        broken down by household size.
        """
        ages = defaultdict(list)
        size_age = [(x['hh_size'], x['age']) for x in self.data.where('first')] \
            if (st is None or et is None) \
            else [(x['hh_size'], x['age']) for x in self.data.where('first & (t>=st) & (t<et)')]
        for cur_case in size_age:
            # store ages for hh of size (1..6, 7+)
            ages[min(cur_case[0], 7)].append(cur_case[1])
        return ages

    def get_hh_age_first_infection_series(self, st=None, et=None, dt=1):
        cur_st = st
        cur_et = min(cur_st + dt, et)
        avg_ages = defaultdict(list)
        while cur_st < et:
            cur_ages = self.get_hh_age_first_infection(cur_st, cur_et)
            for cur_hh_size in range(1, 8):
                avg_ages[cur_hh_size].append(
                    NaN if cur_hh_size not in cur_ages
                    else np.mean(cur_ages[cur_hh_size]))
            #                avg_ages[cur_hh_size].append(np.mean(age_list))
            cur_st = cur_et
            cur_et += dt
        return avg_ages

    #########################################################################
    ## fraction of infections attributable to households

    def get_hh_frac(self, st=None, et=None):
        """
        Get list of ages of first infection in specified time period.
        """
        if st is None or et is None:
            return [x['hh_frac'] for x in self.data]
        else:
            return [x['hh_frac'] for x in self.data.where('(t>=st) & (t<et)')]

    def get_hh_frac_series(self, st=None, et=None, dt=1):
        cur_st = st
        cur_et = min(cur_st + dt, et)
        hh_fracs = []
        while cur_st < et:
            cur_hh_fracs = self.get_hh_frac(cur_st, cur_et)
            hh_fracs.append((np.mean(cur_hh_fracs), np.std(cur_hh_fracs))
                            if len(cur_hh_fracs) > 0 else (np.nan, np.nan))
            cur_st = cur_et
            cur_et += dt
        #print hh_fracs
        return hh_fracs

    def get_hh_hh_frac(self, st=None, et=None):
        """
        Get a list of ages of first infection in specified time period,
        broken down by household size.
        """
        hh_fracs = defaultdict(list)
        size_hh_frac = [(x['hh_size'], x['hh_frac']) for x in self.data] \
            if (st is None or et is None) \
            else [(x['hh_size'], x['hh_frac']) for x in self.data.where('(t>=st) & (t<et)')]
        for cur_case in size_hh_frac:
            # store ages for hh of size (1..6, 7+)
            hh_fracs[min(cur_case[0], 7)].append(cur_case[1])
        return hh_fracs

    def get_hh_hh_frac_series(self, st=None, et=None, dt=1):
        cur_st = st
        cur_et = min(cur_st + dt, et)
        hh_fracs = defaultdict(list)
        while cur_st < et:
            cur_hh_fracs = self.get_hh_hh_frac(cur_st, cur_et)
            for cur_hh_size in range(1, 8):
                hh_fracs[cur_hh_size].append(
                    NaN if cur_hh_size not in cur_hh_fracs
                    else np.mean(cur_hh_fracs[cur_hh_size]))
            #                avg_ages[cur_hh_size].append(np.mean(age_list))
            cur_st = cur_et
            cur_et += dt
        return hh_fracs

    def get_hh_fracs_by_age(self, min_age, max_age, t_step):
        hh_fracs_by_age = defaultdict(list)
        for x in self.data:
            if min_age <= x['age'] < max_age:
                hh_fracs_by_age[x['t'] / t_step].append(x['hh_frac'])
        return sorted([(k, np.mean(v), np.std(v)) for k, v in hh_fracs_by_age.items()])

    def get_hh_counts(self):
        base_counts = defaultdict(list)
        for case in self.data:
            base_counts[case['t']].append(case['hh_size'])

        counts = []
        for tstep in sorted(base_counts.keys()):
            hh_sizes = base_counts[tstep]
            counts.append(
                [tstep,
                 hh_sizes.count(1),
                 hh_sizes.count(2),
                 hh_sizes.count(3),
                 hh_sizes.count(4),
                 hh_sizes.count(5),
                 hh_sizes.count(6),
                 len([x for x in hh_sizes if x > 6])
                ])
        return counts

    def get_hh_props(self):
        counts = defaultdict(list)
        for case in self.data:
            counts[case['t']].append(case['hh_size'])

        props = []
        for tstep in sorted(counts.keys()):
            hh_sizes = counts[tstep]
            props.append(
                [tstep,
                 hh_sizes.count(1) / float(len(hh_sizes)),
                 hh_sizes.count(2) / float(len(hh_sizes)),
                 hh_sizes.count(3) / float(len(hh_sizes)),
                 hh_sizes.count(4) / float(len(hh_sizes)),
                 hh_sizes.count(5) / float(len(hh_sizes)),
                 hh_sizes.count(6) / float(len(hh_sizes)),
                 len([x for x in hh_sizes if x > 6]) / float(len(hh_sizes))
                ])
        return props


    #########################################################################
    ## age distributions

    def get_case_age_dist(self, st=None, et=None, state=None, age_bins=range(101)):

        if not state:
            state = self.entry_state

        if st is None or et is None:
            ages = [x['age'] for x in self.data if x['state'] == state]
        else:
            ages = [x['age'] for x in self.data.where('(t>=st) & (t<et)') if x['state'] == state]
        age_dist = np.histogram(ages, bins=age_bins)[0]
        return age_dist

    def get_case_age_dist_infant(self, st=None, et=None, state=None, bin_days=91, max_age=2):
        # get ages in 

        if not state:
            state = self.entry_state

        if st is None or et is None:
            ages = [x['age'] * 364 + x['age_days']
                    for x in self.data if x['age'] < max_age and x['state'] == state]
        else:
            #ages = [x['age_days'] for x in self.data.where('(t>=st) & (t<et)') if x['age'] < max_age]
            ages = [(x['age'] * 364 + x['age_days'])
                    for x in self.data.where('(t>=st) & (t<et)') if x['age'] < max_age and x['state'] == state]

        # convert ages to months
        ages = np.array(ages) / bin_days
        num_bins = 364 / bin_days * max_age
        age_dist = np.histogram(ages, bins=num_bins, range=(0, num_bins))[0]
        return age_dist

    def get_case_age_dist_current(self, t, st=None, et=None,
                                  age_bins=range(101)):
        """
        As above, but returns distribution of current ages (at time t)
        """

        if st is None or et is None:
            ages = [x['age'] + ((t - x['t']) / 364)
                    for x in self.data]
        else:
            ages = [x['age'] + ((t - x['t']) / 364)
                    for x in self.data.where('(t>=st) & (t<et)')]
        age_dist = np.histogram(ages, bins=age_bins)[0]
        return age_dist

    def bin_age_dist(self, age_dist, age_bins):
        binned_dist = []
        for cur_bin_l, cur_bin_r in zip(age_bins, age_bins[1:] + [101]):
            binned_dist.append(sum(age_dist[cur_bin_l:cur_bin_r]))
        return binned_dist

    def get_mean_age_dist_bin(self, age_bins, st, et):
        """
        Get mean binned age distribution over specified period.
        """
        binned_age_dists = []
        for x in self.age_dists.where('(t>=st) & (t<et)'):
            binned_age_dists.append(self.bin_age_dist(x['dist'], age_bins))
        return np.mean(binned_age_dists, 0)

    def get_mean_age_dist(self, st, et):
        return np.mean([x['dist'] for x in self.age_dists.where('(t>=st) & (t<et)')], 0)

    def get_mean_age_dist_month(self, st, et):
        return np.mean([x['dist'] for x in self.age_dists_month.where('(t>=st) & (t<et)')], 0)

    #########################################################################
    ## household size distributions

    def get_mean_hh_size_dist(self, st, et):
        return np.mean([x['dist'] for x in self.hh_size_dists.where('(t>=st) & (t<et)')], 0)

    #########################################################################
    ## overall incidence

    def get_annual_incidence(self, times, dt):
        sizes = self.get_mean_pop_size_time(times['st'], times['et'], dt)
        cases = self.get_new_case_counts_by_time(times['st'], times['et'], dt)
        return np.array(cases) / np.array(sizes) / dt * times['t_per_year']  # last bit makes it annual (?)

    #########################################################################
    ## incidence by age

    def get_incidence_by_age_infant(self, age_dist, st, et, max_age):
        """
        Infant version of below; note curently doesn't handle multiple infectious states.
        Could be extended to do so by adding further parameter to get_case_age_dist...
        """

        filtered = self.filter_data_by_state()
        incs = {}
        for k, v in filtered.items():
            ages = self.get_case_age_dist_infant(st, et, k, 28, max_age)
            duration = max((et - st) / float(self.t_per_year), 1.0)
            incs[k] = [(float(x) / (y * duration) if y > 0 else 0.0) for x, y in zip(ages, age_dist)]
        return incs

    def get_incidence_by_age(self, age_dist, st, et):
        """
        Get annual incidence broken down by age, calculated over the 
        specified interval.  Age distribution should be specified in 
        non-normalised form (i.e., number of individuals in each class,
        rather than proportion).

        Returns incidences in a dictionary keyed by actual disease state, to
        allow for handling of multiple infectious states (e.g., primary and
        secondary).

        TODO: 
        * probably not flexible enough to handle multi-year bins atm (?)
        * something else...
        """

        filtered = self.filter_data_by_state()
        incs = {}
        for k, v in filtered.items():
            ages = self.get_case_age_dist(st, et, k)
            duration = max((et - st) / float(self.t_per_year), 1.0)
            incs[k] = [(float(x) / (y * duration) if y > 0 else 0.0) for x, y in zip(ages, age_dist[:-1])]
        return incs

    def get_incidence_by_age_snapshots(self, st, et, num_snapshots):
        """
        Get series of incidence by age distributions at evenly spaced time intervals
        between st and et.
        """
        dt = (et - st) / num_snapshots
        inc_age_dists = []
        for i in range(num_snapshots):
            cur_st = st + (i * dt)
            cur_et = cur_st + dt
            age_dist = self.get_mean_age_dist(cur_st, cur_et)
            cur_inc = self.get_incidence_by_age(age_dist, cur_st, cur_et)
            inc_age_dists.append(cur_inc[self.entry_state])
        return inc_age_dists

    def get_incidence_by_age_infant_snapshots(self, st, et, num_snapshots):
        """
        NB: this seems a bit broken!!! (fixed now)
        """
        dt = (et - st) / num_snapshots
        inc_age_infant_dists = []
        for i in range(num_snapshots):
            cur_st = st + (i * dt)
            cur_et = cur_st + dt
            age_dist_quarters = self.get_mean_age_dist_month(st, et)
            cur_inc = self.get_incidence_by_age_infant(age_dist_quarters, cur_st, cur_et, 1)
            inc_age_infant_dists.append(cur_inc[self.entry_state])
        return inc_age_infant_dists

    def get_incidence_by_age_bin(self, age_bins, st, et, dt):
        """
        Get incidence by age curves, for specified age bins, over specified intervals
        (st, et = start time, end time; dt = time step size).
        
        Uses closest available stored age distribution.
        """
        incs = []
        for cur_st in range(st, et, dt):
            #print cur_st
            cur_et = cur_st + dt

            ages = [x['age'] for x in self.data.where('(t>=cur_st) & (t<cur_et)')]
            d, b = np.histogram(ages, bins=age_bins + [101], normed=False)
            #print d[:5]
            duration = dt / self.t_per_year
            age_dist = self.get_mean_age_dist_bin(age_bins, cur_st, cur_et)
            #print age_dist[:5]
            incs.append([(float(x) / (y * duration)) if y > 0 else 0 for x, y in zip(d, age_dist)])
        return incs

    #########################################################################
    ## incidence by household size (and type)

    def get_incidence_by_hh_size(self, ind_hh_dist, st, et, max_size):
        """
        Get annual incidence broken down by household size.  ind_hh_dist is 
        distribution of individuals by household size (again, non-normalised).
        
        max_size: aggregate all households of this size or above
        """
        filtered = self.filter_data_by_state()
        incs = {}
        for k, v in filtered.items():
            hh_sizes = [x['hh_size'] for x in v if st < x['t'] < et]
            d, b = np.histogram(hh_sizes, bins=len(ind_hh_dist),
                                range=(0, len(ind_hh_dist)), normed=False)
            binned_d = list(d[0:max_size]) + [sum(d[max_size:])]
            binned_dist = list(ind_hh_dist[:(max_size - 1)]) + [sum(ind_hh_dist[max_size - 1:])]
            duration = max((et - st) / float(self.t_per_year), 1.0)
            incs[k] = [(float(x) / ((y[0] + 1) * y[1] * duration) if y[1] > 0 else 0.0) for x, y in
                       zip(binned_d, enumerate(binned_dist))]
        return incs

    def get_incidence_by_hh_size_snapshots(self, st, et, num_snapshots, max_hh_size=10):
        """
        NB: this seems a bit broken!!! (fixed now)
        """
        dt = (et - st) / num_snapshots
        inc_hh_dists = []
        for i in range(num_snapshots):
            cur_st = st + (i * dt)
            cur_et = cur_st + dt
            hh_size_dist = self.get_mean_hh_size_dist(cur_st, cur_et)
            cur_inc = self.get_incidence_by_hh_size(hh_size_dist, cur_st, cur_et, max_hh_size)
            inc_hh_dists.append(cur_inc[self.entry_state])
        return inc_hh_dists

    def get_incidence_by_hh_size_time(self, st, et, dt):
        incs = []
        for cur_st in range(st, et, dt):
            cur_et = cur_st + dt
            hh_size_dist = self.get_mean_hh_size_dist(cur_st, cur_et)
            cur_incs = self.get_incidence_by_hh_size(hh_size_dist, cur_st, cur_et, 10)
            incs.append(cur_incs[self.entry_state])
        return incs

    def get_incidence_by_hh_type(self, ind_hh_type_dist, st, et):
        """
        Get annual incidence broken down by household type.  ind_hh_type_dist 
        is distribution of individuals by household type 
        (again, non-normalised).
        """

        filtered = self.filter_data_by_state()
        incs = {}
        for k, v in filtered.items():
            hh_types = ['kids' if x['hh_type'].endswith('kids') else 'nokids'
                        for x in v if st < x['t'] < et and 18 < x['age']]
            counts = (hh_types.count('kids'), hh_types.count('nokids'))
            duration = max((et - st) / float(self.t_per_year), 1.0)
            incs[k] = [float(x) / (y * duration) for x, y in \
                       zip(counts, ind_hh_type_dist)]
        return incs


    #########################################################################
    ## demographic stuff (that probably ought be in its own observer)

    def get_pop_size_time(self):
        time_values = [x['t'] for x in self.age_dists]
        pop_sizes = [sum(x['dist']) for x in self.age_dists]
        return time_values, pop_sizes

    def get_mean_pop_size_time(self, st, et, dt):
        sizes = []
        for cur_st in range(st, et, dt):
            cur_et = cur_st + dt
            pop_sizes = [sum(x['dist']) for x in self.age_dists.where('(t >= cur_st) & (t < cur_et)')]
            sizes.append(np.mean(pop_sizes))
        return sizes

    def get_age_dists_time(self, t_snapshots=None):
        age_dists = []
        if not t_snapshots:
            t_snapshots = self.age_dists.col('t')

        for cur_t in t_snapshots:
            raw_dist = [x['dist'] for x in self.age_dists.where('t==cur_t')][0]
            total = sum(raw_dist)
            prop_dist = [x / float(total) for x in raw_dist]
            age_dists.append(prop_dist)
        return age_dists

    def get_hh_size_dists_time(self, t_snapshots=None, max_hh_size=10):
        hh_size_dists = []
        if not t_snapshots:
            t_snapshots = self.age_dists.col('t')

        for cur_t in t_snapshots:
            raw_dist = [x['dist'] for x in self.hh_size_dists.where('t==cur_t')][0]
            total = sum(raw_dist)
            prop_dist = [x / float(total) for x in raw_dist[:(max_hh_size - 1)]] + [
                sum(raw_dist[(max_hh_size - 1):]) / float(total)]
            hh_size_dists.append(prop_dist)
        return hh_size_dists

    #########################################################################
    ## utility functions

    def get_snapshot_times(self, num_snapshots):
        time_values = [x['t'] for x in self.age_dists]
        length = float(len(time_values))
        t_snapshots = [time_values[int(ceil(x * length / num_snapshots))] for x in range(num_snapshots)]
        return t_snapshots

    def export_as_csv(self, filename):
        subset = [self.data.col('t'), self.data.col('t_E'), self.data.col('ID'), self.data.col('age'),
                  self.data.col('hh_id'), self.data.col('hh_size'), self.data.col('hh_source'),
                  self.data.col('source_id'), self.data.col('ext_contacts'), self.data.col('ext_infections')]
        s_array = np.array(subset)
        s_array = np.transpose(s_array)
        np.savetxt(filename, s_array, delimiter=',')

    #########################################################################
    ### OUTPUT functions ###
    def output_all(self, p, times):

        # - #  -- demography outputs -- # - # - # - # - # - # - # - # - # - # - #

        # A hack to dump the population size (obviously only of interest in a growing population!)
        # eventually this should be handled by a demography observer that would also store household
        # and age distributions (and could potentially be used by case observer for denominator values)

        t_snapshots = []
        if p['dyn_rates']:
            print "Generating demographic plots"
            output_timeseries(os.path.join(p['prefix'], 'demog_pop_size.png'), times,
                              [self.get_pop_size_time()[1]], 'Population Size')

            t_snapshots = self.get_snapshot_times(5)
            age_dists = self.get_age_dists_time(t_snapshots)

            num = 0.0
            denom = 0.0
            for cur_age, cur_count in enumerate(age_dists[0]):
                num += cur_age * cur_count
                denom += cur_count
            print num / denom

            plot_statistics(os.path.join(p['prefix'], 'demog_age_dists.png'), range(len(age_dists[0])),
                            age_dists, 'Age', 'Proportion',
                            series_labels=['t=%d' % (x / p['t_per_year']) for x in t_snapshots])

            hh_size_dists = self.get_hh_size_dists_time(t_snapshots, max_hh_size=10)
            plot_statistics(os.path.join(p['prefix'], 'demog_hh_size_dists.png'), range(1, len(hh_size_dists[0]) + 1),
                            hh_size_dists, 'Household size', 'Proportion',
                            series_labels=['t=%d' % (x / p['t_per_year']) for x in t_snapshots])

            age_bins = [0, 1, 5, 10, 20, 40, 60]
            age_dists_all = zip(*[self.bin_age_dist(x, age_bins) for x in self.get_age_dists_time()])

            output_stacked_bars_timeseries(os.path.join(p['prefix'], 'demog_age_props.png'),
                                           times, age_dists_all, 'Proportion of population',
                                           labels=['<%d' % age_bins[0]] + ['%d-%d' % (x, y) for x, y in
                                                                           zip(age_bins[1:], age_bins[2:] + [100])])

            output_stacked_bars_timeseries(os.path.join(p['prefix'], 'demog_hh_size_props.png'),
                                           times, zip(*self.get_hh_size_dists_time(max_hh_size=7)), 'Proportion',
                                           labels=['%d' % x for x in range(1, 7)] + ['7+'])

        # - #  -- timeseries outputs -- # - # - # - # - # - # - # - # - # - # - #

        # output case count per time period
        # TODO: make options for daily, weekly, etc.
        print "Generating case timeseries plot"
        output_timeseries(os.path.join(p['prefix'], 'cases_new.png'), times,
                          self.get_new_case_counts_by_time(
                              times['st'], times['et'], 1),
                          'New cases')

        print "Generating annual incidence plot"
        incs = self.get_annual_incidence(times, 10 * self.t_per_year)
        output_timeseries(os.path.join(p['prefix'], 'cases_total_incidence.png'), times,
                          incs, 'Incidence')

        pts_per_year = 0.1

        # output househould-attributable fractions
        print "Generating hh fraction plot"
        hh_fracs, hh_fracs_err = zip(*self.get_hh_frac_series(
            times['st'], times['et'], int(p['t_per_year'] / pts_per_year)))
        output_timeseries(os.path.join(p['prefix'], 'cases_hh_fracs.png'), times,
                          hh_fracs, 'Household fraction')  # , y_errs=hh_fracs_err)

        print "Generating hh fraction by hh size plot"
        values = self.get_hh_hh_frac_series(times['st'], times['et'], int(times['t_per_year'] / pts_per_year))
        series = [values[x] for x in sorted(values)]
        labels = ['%d' % x for x in sorted(values)]
        labels[-1] += '+'
        output_timeseries(os.path.join(p['prefix'], 'cases_hh_frac_hh.png'),
                          times, series, 'Household fraction', labels=labels)

        # output average age of first infection
        print "Generating avg age at first infection plot"
        avg_age, avg_age_err = zip(*self.get_age_first_infection_series(
            times['st'], times['et'], int(times['t_per_year'] / pts_per_year)))
        output_timeseries(os.path.join(p['prefix'], 'cases_age_first_infection.png'), times,
                          avg_age, 'Age')  # , y_errs=avg_age_err)

        print "Generating avg age at first infection by hh size plot"
        values = self.get_hh_age_first_infection_series(
            times['st'], times['et'], int(times['t_per_year'] / pts_per_year))
        series = [values[x] for x in sorted(values)]
        labels = ['%d' % x for x in sorted(values)]
        labels[-1] += '+'
        output_timeseries(os.path.join(p['prefix'], 'cases_age_first_infection_hh.png'),
                          times, series, 'Age', labels=labels)

        # - #  -- incidence outputs -- # - # - # - # - # - # - # - # - # - # - #

        # age incidence
        print "Generating incidence by age plots"
        age_dist = self.get_mean_age_dist(times['st'], times['et'])
        age_inc_dat = self.get_incidence_by_age(age_dist, times['st'], times['et'])

        incs_snapshots = [age_inc_dat[self.entry_state]]

        if p['dyn_rates']:
            incs_snapshots += self.get_incidence_by_age_snapshots(times['st'], times['et'], 5)

        plot_statistics(os.path.join(p['prefix'], 'cases_incidence_by_age_snapshots.png'),
                        range(len(incs_snapshots[0])), incs_snapshots, 'Age', 'Incidence', xlogscale=True,
                        series_labels=['Overall'] + ['t=%d' % (x / p['t_per_year']) for x in t_snapshots])

        age_bins = [0, 1, 5, 10, 20, 40, 60]
        incs = self.get_incidence_by_age_bin(age_bins, times['st'], times['et'], times['t_per_year'])

        output_timeseries(os.path.join(p['prefix'], 'cases_incidence_by_age_time.png'),
                          times, np.transpose(np.array(incs)), 'Incidence',
                          labels=['%d-%d' % (x, y) for x, y in zip(age_bins, age_bins[1:] + [100])])

        # household size incidence
        print "Generating incidence by hh size plots"
        max_hh_size = 7
        hh_size_dist = self.get_mean_hh_size_dist(times['st'], times['et'])
        hh_inc_dat = self.get_incidence_by_hh_size(hh_size_dist, times['st'], times['et'], max_hh_size + 1)

        # start with overall incidence
        incs_snapshots = [hh_inc_dat[self.entry_state]]

        if p['dyn_rates']:
        # add on incidence across evenly spaced intervals
            incs_snapshots += self.get_incidence_by_hh_size_snapshots(times['st'], times['et'], 5,
                                                                  max_hh_size=max_hh_size + 1)

        plot_statistics(os.path.join(p['prefix'], 'cases_incidence_by_hh_size_snapshots.png'),
                        range(len(incs_snapshots[0])), incs_snapshots, 'Household size', 'Incidence',
                        series_labels=['Overall'] + ['t=%d' % (x / p['t_per_year']) for x in t_snapshots])

        incs = self.get_incidence_by_hh_size_time(times['st'], times['et'], times['t_per_year'])
        output_timeseries(os.path.join(p['prefix'], 'cases_incidence_by_hh_size_time.png'),
                          times, np.transpose(np.array(incs)), 'Incidence',
                          labels=['%d' % (x + 1) for x in range(max_hh_size + 1)])


