import os, sys, time, multiprocessing, logging
from random import Random
import numpy as np
from itertools import product, combinations, chain
from collections import defaultdict

from mpi4py import MPI
#from pympler import summary, muppy, tracker

from population.utils import create_path

# An iterator object for parallelising across multiple parameter combinations
class ParamComboIt(object):
    def __init__(self, p, p_values, disease_type=None, cmatrix=None, use_seeds=True):
        self.p = p
        self.disease_type = disease_type
        self.cmatrix = cmatrix
        # create a RNG for seeds (we will use same seeds for each param combo)
        # the use_seeds flag allows seeds to be ignored
        if use_seeds:
            seed_rng = Random() if p['random_seed'] else Random(p['seed'])
            self.seeds = [seed_rng.randint(0, 99999999) for x in range(p['num_runs'])]
        else:
            self.seeds = [p['seed']]
        self.base_prefix = p['prefix']
        self.p_values = p_values
        # generate all necessary parameter combinations (indexed by sample_ID)
        self.combos = [a for a in product(*[range(len(x['values'])) for x in p_values])]
    
        self.combo_index = 0
        self.seed_index = 0

    def print_combos(self):
        for cur_combo in self.combos:
            print "".join(['{0:>10}'.format(self.p_values[i]['values'][x]) \
                    for i, x in enumerate(cur_combo)])

    def __iter__(self):
        return self

    def next(self):
        if self.combo_index == len(self.combos):
            raise StopIteration
        else:
            #print "combo:", self.combo_index + 1, ", (", len(self.combos), ")"
            #print "seed:", self.seed_index + 1, ", (", len(self.seeds), ")"
                            
            # make a local copy of params to modify
            p = self.p.copy()
            # set parameter values for this combination
            cur_combo = self.combos[self.combo_index]
            output_dir = "params_"
            param_strings = []
            for i, cur_p_name in enumerate([x['name'] for x in self.p_values]):
                p[cur_p_name] = self.p_values[i]['values'][cur_combo[i]]
                param_strings.append("%s=%f" % (cur_p_name, 
                        self.p_values[i]['values'][cur_combo[i]]))
            output_dir += "_".join(param_strings)
            p['prefix'] = os.path.join(self.base_prefix, output_dir)
            # only use seed directories if running more than one seed
            if len(self.seeds) > 1:
                p['prefix'] = os.path.join(p['prefix'], 'seed_%d'%self.seed_index)
                
            cur_seed = self.seeds[self.seed_index]
            #print "cur_seed", cur_seed
            
            if self.seed_index < len(self.seeds) - 1:
                #print "incrementing seed"
                self.seed_index += 1
            else:
                if self.combo_index < len(self.combos):
                    #print "incrementing combo"
                    self.combo_index += 1
                    #print "resetting seed"
                    self.seed_index = 0
            
            return {
                    'disease_type': self.disease_type,
                    'cmatrix': self.cmatrix,
                    'seed': cur_seed,
                    'p': p,
                    'p_string' : ";".join(param_strings)
                    }


class SweepResults(object):
    """
    Stores a multidimensional array mapping parameter combinations to result indices.
    The aim is to make it straightforward to pull out one- and two-dimensional parameter
    maps.
    """
    
    def __init__(self, sweep_values, combos, num_seeds):
        self.sweep_values = sweep_values
        self.num_seeds = num_seeds
        # create a mapping from a parameter hypercube to an output
        self.indices = np.zeros(tuple([len(x['values']) for x in sweep_values]),dtype=int)
        # populate array with indices
        for i, cc in enumerate(combos):
            pt = "self.indices" + "".join(["[cc[%d]]"%x for x in range(len(cc))]) + "=%d"%i
            exec(pt)
        # create a mapping from parameter name to axis index
        self.index_map = dict([(x['name'], i) for i, x in enumerate(sweep_values)])
    
    
    def retrieve_results(self, fixed={}):
        """
        retrieve results:          
        - provide a list of (param, value) pairs.
        - use index_map and sweep_values to get (param_index, value_index) pairs
        - build a string representing array access (assume ':' for any non-specified params)
        - execute and return result subset
        """

        pt = "subset = self.indices["
        pt += ",".join([
            ":" if x['name'] not in fixed else "%d"%x['values'].index(fixed[x['name']]) \
            for x in self.sweep_values])
        pt += "]"
        exec(pt)
        # if sweep is 1D, need to force return value to be a list (of length one)
        return [subset] if isinstance(subset, int) else subset.tolist()
    
    
    def retrieve_results_sweep(self, sweep):
        """
        Return a list of results obtained by iteratively holding each value of the sweep param fixed,
        and sweeping over the remaining parameters.
        NB: for a param hypercube of n dimensions, will return slices of dimension n-1
        eg, for a 2D set of param combinations, will return a list of results
            for a 3D set of param combiantions, will return a matrix of results, etc.
            
        TODO: should return values as well as results (as per below)
        """
        return [self.retrieve_results({sweep: x}) \
                for x in self.sweep_values[self.index_map[sweep]]['values']]
    
    
    def retrieve_results_multi_sweep(self, cur_param):
        """
        Return a list of results obtained by holding sweeping over cur_p for all combinations of other parameters.
        NB: will *always* return a *list* of results (one per parameter combination).
        """
        # generate combinations of remaining sweep parameters
        other_names = [x['name'] for x in self.sweep_values if x['name'] != cur_param]
        other_combos = [a for a in product(*[range(len(x['values'])) \
                for x in self.sweep_values if x['name'] != cur_param])]

        values = []
        results = []
        
        # for each combination of other sweep parameters
        for cur_combo in other_combos:
            # build a dictionary of current values
            cur_values = {}
            for i, cur_other in enumerate(other_names):
                cur_values[cur_other] = self.sweep_values[self.index_map[cur_other]]['values'][cur_combo[i]]
            values.append(cur_values.values())
            results.append(self.retrieve_results(cur_values))
            
        return values, results
    
#    def retrieve_results_multi_sweep(self, sweep1, sweep2, f={}):
#        """
#        """    
#        print sweep1, sweep2
#        
#        return chain([self.retrieve_results_sweep(sweep1, fixed=dict(chain({sweep2: x}.iteritems(), f.iteritems()))) \
#                for x in self.sweep_values[self.index_map[sweep2]]['values']])
                
    
    
def go_sweep(p, sweep_values, disease_type, cmatrix, process_fn, **kwargs):
    """
    Performs a multi dimensional exploration of parameter space.
   
    NB: process_fn should NOT return a disease object -- it can't be pickled!
    Rather it should return, e.g., a dictionary of stats for procesing.
    """

    # produce a generator object for all possible parameter/seed combinations
    param_combo_it = ParamComboIt(p, sweep_values, disease_type, cmatrix)
    sweep_results = SweepResults(sweep_values, param_combo_it.combos, p['num_runs'])    

    # show parameter combinations
    print "@@_go_sweep: About to sweep over the following parameter", \
            "combinations using function %s:" % process_fn.__name__
    print "".join(["%10s" % x['name'] for x in sweep_values])
    param_combo_it.print_combos()

    #multiprocessing.log_to_stderr(multiprocessing.SUBDEBUG)
    
    # use either specific number of processors, or all available
    n_procs = min(kwargs.get('n_procs', multiprocessing.cpu_count()),
                  multiprocessing.cpu_count())
    pool = multiprocessing.Pool(n_procs)

    # a list of simulation outputs in which each entry corresponds to a 
    # particular parameter combination, with the same index as in p_combos
    output = []
    r = pool.map_async(process_fn, param_combo_it, callback=output.append)
    r.wait()

    if len(output) > 0:
        output = output[0] # again, why is this necessary?
    
    return sweep_results, output
        

def go_sweep_mpi(p, sweep_values, disease_type, cmatrix, process_fn):
    """
    As above, but uses mpi4py rather than multiprocessing
    """
    
    # produce a generator object for all possible parameter/seed combinations
    param_combo_it = ParamComboIt(p, sweep_values, disease_type, cmatrix)
    sweep_results = SweepResults(sweep_values, param_combo_it.combos, p['num_runs'])    

    # show parameter combinations
    print "@@_go_sweep: About to sweep over the following parameter", \
            "combinations using function %s:" % process_fn.__name__
    print "".join(["%10s" % x['name'] for x in sweep_values])
    param_combo_it.print_combos()

    comm = MPI.COMM_WORLD
    rank = comm.Get_rank()
    size = comm.Get_size()
    
    if rank == 0:
        params_all = [x for x in param_combo_it]
        params_slices = [[] for _ in range(size)]
        for i, x in enumerate(params_all):
            params_slices[i % size].append(x)
    else:
        params_slices = None
    
    params_local = comm.scatter(params_slices, root=0)
    results_local = [process_fn(cur_params) for cur_params in params_local]
    results_combined = comm.gather(results_local, root=0)

    if rank == 0:
        output = [x for y in results_combined for x in y]
        return sweep_results, output
    else:
        return None, None



def aggregate_over_seeds(output, num_seeds, lims=[]):
    """
    output is a list of dictionaries of output values, keyed by output name
    for each output name that refers to an integer or float output value,
    calculate the mean and standard deviation over multiple runs with the same parameters  
    
    lims can be used to only include values within the specified range (ie to exclude error values)
    (this is a bit hacky, could be more flexible)
    
    Should now handle lists (of ints and floats at least) as well, calculating mean/stdev over appropriate axis.
    """

    # for a lot of calculations I don't want to keep printing "Aggregation!"
    #print "Aggregation!"
    agg_output = []
    for i in range(0, len(output), num_seeds):
        cur_output = {}
        for k in output[i].keys():
            if isinstance(output[i][k], (int, float, list, tuple, np.ndarray)):
                if lims:
                    cur_values = [x[k] for x in output[i:i+num_seeds] if lims[0] <= x[k] <= lims[1]]
                else:
                    cur_values = [x[k] for x in output[i:i+num_seeds]]
            
                cur_output['mean_%s'%k] = np.mean(cur_values, axis=0)
                cur_output['sd_%s'%k] = np.std(cur_values, axis=0)
        agg_output.append(cur_output)
    return agg_output
    
    

