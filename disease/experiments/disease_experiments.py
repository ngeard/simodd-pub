### TODO: possible merge (remaining) functions here into param_sweeps
import inspect
import tables as tb
from scipy import stats
from math import log

from random import Random
from disease.general.sim_epi import SimEpi
from disease.experiments.output_disease import *

from population.utils import create_path

from disease.experiments.param_sweeps import aggregate_over_seeds

def convert_old(p, disease_type, cmatrix, cur_seed, sim_type=SimEpi, verbose=False, p_string=None):
    rng = Random(cur_seed)
    disease_fname = os.path.join(p['prefix'], 'disease.hd5')

    if not os.path.isfile(disease_fname):
        print "Problem: disease (%sseed=%d)..." % (p_string + ";" if p_string else None, cur_seed), "does not exist!"
    else:
        disease = disease_type(p, cmatrix, rng, disease_fname, mode='a')
        disease.store_params(p)
        disease.done(True)


def go_single(p, disease_type, cmatrix, cur_seed, sim_type=SimEpi, verbose=False, p_string=None):
    """
    Run a single simulation (or load if previously run).
    
    Arguments:
    p               : parameter structure
    disease_type    : disease object to simulate
    cmatrix         : contact matrix
    cur_seed        : random seed for current experiment
    sim_type        : simulation class to use (current options are SimEpi or SimBDI)
    verbose         : write output to terminal
    p_string        : an optional string containing (e.g.) parameter values to be written to terminal (for information purposes)
    """

    create_path(p['prefix'])
    rng = Random(cur_seed)
    disease_fname = os.path.join(p['prefix'], 'disease.hd5')

    # a check to remove invalid files; NB: will not remove files from partial/incomplete runs (use x for that)
    if os.path.isfile(disease_fname) and not tb.isPyTablesFile(disease_fname):
        os.remove(disease_fname)

    print p['overwrite']
    # load disease if output file already exists, otherwise run simulation
    if os.path.isfile(disease_fname) and not p['overwrite']:
        print "@@_go_single: loading existing disease (%s%sseed=%d)..." % (
            disease_type, p_string + ";" if p_string else None, cur_seed)
        print "NB: to overwrite existing disease output, rerun with 'x' switch (eg, 'python main.py s x')"
        try:
            disease = disease_type(p, cmatrix, rng, disease_fname, mode='r')
        except tb.exceptions.NoSuchNodeError:
            # this is thrown when requested observers don't exist in disease file...
            disease.done(False)
            print "existing disease not complete... rerunning"
        else:
            if disease.is_complete():
                disease.done()
                return
            else:
                disease.done(False)
                print "existing disease not complete... rerunning"

    print "@@_go_single: running simulation (%sseed=%d)..." % (p_string + ";" if p_string else "", cur_seed)
    # create and set up simulation object
    disease = disease_type(p, cmatrix, rng, disease_fname, mode='w')
    sim = sim_type(p, disease, rng)
    #sim.setup(verbose=verbose)

    start_year = p['burn_in'] + p['epi_burn_in']
    #year_list = [y-x for x, y in zip(p['years'][:-1], p['years'][1:])]
    year_list = [(start_year + x, start_year + y) for x, y in zip(p['years'][:-1], p['years'][1:])]

    start_index = 0
    # starting at end, test for existence of each checkpoint, to identify where we need to start from
    for i, cur_years in enumerate(reversed(year_list)):
        cp_filename = os.path.join(p['prefix'], 'cp_%d-%d.pop' % (cur_years[0], cur_years[1]))
        print "Testing for checkpoint: %s" % cp_filename
        # if this checkpoint exists, break and run forward from here, otherwise check for previous...
        if os.path.isfile(cp_filename) and not p['overwrite']:
            start_index = len(year_list) - i
            break

    # if final checkpoint exists, our work here is done...
    # this implies that disease should exist, so should never occur in practice
    if start_index == len(year_list):
        sim.disease.done(True)
        return

    # if none of our checkpoints existed, run setup to make sure populations exist
    if start_index == 0:
        sim.setup(verbose=verbose)
        start_index = 1

    # load/run each checkpoint in turn (starting with latest existing one to get population loaded... need fixing!)
    for cur_years in year_list[start_index - 1:]:
        # for efficiency, only turn on observers for final period
        observers = (cur_years == year_list[-1])
        cp_prefix = p['prefix']  #if (i == len(year_list) - 1) else p['epi_prefix']
        sim.run_cp(cur_years[0], cur_years[1] - cur_years[0], observers, verbose, cp_prefix)
        #start_year += cur_years
    print "\t... simulation done!"
    sim.disease.done(True)
    #disease = sim.disease

    #return disease


def output_single(p, disease):
    print "Processing data and generating output plots..."
    times = get_times(p)  #, end_time=75, days_per_tick=15)
    create_path(p['prefix'])

    # Try calling output all for each observer
    for cur_label, cur_observer in disease.observers.iteritems():
        if inspect.ismethod(cur_observer.output_all):
            #try:
            print "Processing %s output" % cur_label
            cur_observer.output_all(p, times)
        else:
            #except Exception:
            print "Observer %s doesn't implement output_all!" % cur_label

    return True



