import os

from matplotlib.colors import LogNorm


try:
    import brewer2mpl as brew

    colors = ['k'] + brew.get_map('Set1', 'qualitative', 9).mpl_colors
except ImportError:
    colors = ['b', 'r', 'g', 'm', 'c', 'k', '0.5']
from scipy.optimize import fmin

from output_statistics import *


## helper / processing functions # - # - # - # - # - # - # - # - # - # - #

def get_times(p, start_time=0, end_time=0, days_per_tick=50):
    """
    si and ei are start_index and end_index, i.e., start and end times in units of *timesteps*
    st and et are start_time and end_time, i.e., start and end times in units of *days*
    """
    start_year = 0
    final_year = p['years'][-1] if end_time is 0 else p['years'][-2] + end_time / 364.0
    #    print "final_year", final_year
    end_year = final_year - p['years'][-2]
    times = {
        'si': int(p['t_per_year'] * start_year),
        'ei': int(p['t_per_year'] * end_year),
        'st': int((p['burn_in'] + p['epi_burn_in'] \
                   + p['years'][-2]) * p['t_per_year']) - start_time,
        'et': int((p['burn_in'] + p['epi_burn_in'] \
                   + final_year) * p['t_per_year']) - start_time,
        't_per_year': p['t_per_year'],
        'years_per_tick': 1,
        'days_per_tick': days_per_tick
    }
    #    print p['burn_in'], p['epi_burn_in'], p['years']
    #    print times
    return times


def convert_counts_to_props(bins, cutoffs):
    # calculate sum for each age category, and convert counts to proportions
    sums = np.zeros(len(cutoffs))
    for st in bins.values():
        for i, v in enumerate(st):
            sums[i] += v
    props = {}
    for k in bins:
        props[k] = bins[k] / sums * 100
    return props


## over time # - # - # - # - # - # - # - # - # - # - # - # - # - # - # - # - #



def output_timeseries(ofile, times, series, ylabel, y_errs=None, labels=None,
                      logscale=False, xmin=None, ymin=None, ymax=None, ylines=None):
    fig = plt.figure()
    ax = fig.add_subplot(111)

    plot_timeseries_ax(ax, times, series, ylabel, y_errs, labels, logscale,
                       xmin, ymin, ymax, ylines)

    fig.savefig(ofile)


def plot_timeseries_ax(ax, times, series, ylabel, y_errs=None, labels=None,
                       logscale=False, xmin=None, ymin=None, ymax=None, ylines=None, align=None,
                       x_offset=0, legend_title=None, legend_loc='upper left', plot_title=None,
                       linestyle='-'):
    # wrap series if only a single list passed
    if not (isinstance(series[0], list) \
                    or isinstance(series[0], np.ndarray) \
                    or isinstance(series[0], tuple)):
        series = [series]

    # compute xvalues (possibly also allow to passed explicitly?)
    if align == 'centre':
        offset = (times['et'] - times['st']) / (len(series[0]) + 1) / 2.0
        #print times['st'], times['et'], offset
        xvals = np.linspace(times['st'] + offset, times['et'] - offset, len(series[0]))
        #print xvals
    else:
        xvals = np.linspace(times['st'], times['et'], len(series[0]))

    plot_lines(ax, xvals, series, '', ylabel, y_errs, labels, legend_title, legend_loc, linestyle)

    if ymax:
        ax.set_ylim(ymax=ymax)
    if ymin is not None:
        ax.set_ylim(ymin=ymin)
    if xmin:
        times['st'] += xmin

    if ylines:
        for cur_y in ylines:
            ax.hlines(cur_y, ax.get_xlim()[0], ax.get_xlim()[1], colors='k', linestyle='dotted')

    #print x_offset
    setup_x_axis(ax, times, x_offset)
    if plot_title:
        ax.set_title(plot_title, fontsize=16)

    if logscale:
        ax.set_yscale('log')


def output_stacked_bars_timeseries(ofile, times, series, ylabel, labels=None):
    fig = plt.figure()
    ax = fig.add_subplot(111)

    plot_stacked_bars_timeseries_ax(ax, times, series, ylabel, labels=labels)

    fig.savefig(ofile)


def plot_stacked_bars_timeseries_ax(ax, times, series, ylabel, x_offset=0, labels=None, legend_title=None,
                                    plot_title=None):
    xvals = np.linspace(times['st'], times['et'], len(series[0]))

    y = np.row_stack(series)
    y_stack = np.cumsum(y, axis=0)

    plot_stacked_area(ax, xvals, y_stack, '', ylabel, labels, legend_title, plot_title)

    setup_x_axis(ax, times, x_offset)


def output_general(ofile, xvals, yvals, xlabel, ylabel, y_errs=None, labels=None,
                    logscale=False, title=None, xmin=None, xmax=None, ymin=None, ymax=None, ylines=None):
    """
    General plotting function almost identical to output_timeseries except can plot over any xvals so
    doesn't need the special 'times' passed in as input
    """

    fig = plt.figure()
    ax = fig.add_subplot(111)

    plot_lines(ax, xvals, yvals, xlabel, ylabel, y_errs, labels)

    if xmin is not None:
        ax.set_xlim(xmin=xmin)
    if xmax:
        ax.set_xlim(xmax=xmax)
    if ymin is not None:
        ax.set_ylim(ymin=ymin)
    if ymax:
        ax.set_ylim(ymax=ymax)

    if ylines:
        for cur_y in ylines:
            ax.hlines(cur_y, ax.get_xlim()[0], ax.get_xlim()[1], colors='k', linestyle='dotted')

    if logscale:
        ax.set_yscale('log')

    if title:
        ax.set_title(title)

    fig.savefig(ofile)


def output_scatter(ofile, xvals, yvals, xlabel, ylabel, labels=None,
                logscale=False, title=None, xmin=None, xmax = None, ymin=None, ymax=None, ylines=None):
    """
    Function to output a scatter plot of xvals and yvals which are both lists of equal length. Doesn't
    include ability to plot error bars (as for single points)
    
    Nic might have a function that does the same thing
    """

    fig = plt.figure()
    ax = fig.add_subplot(111)

    ax.scatter(xvals, yvals)

    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)

    if xmin is not None:
        ax.set_xlim(xmin=xmin)
    if xmax:
        ax.set_xlim(xmax=xmax)
    if ymin is not None:
        ax.set_ylim(ymin=ymin)
    if ymax:
        ax.set_ylim(ymax=ymax)

    if ylines:
        for cur_y in ylines:
            ax.hlines(cur_y, ax.get_xlim()[0], ax.get_xlim()[1], colors='k', linestyle='dotted')

    if logscale:
        ax.set_yscale('log')

    if title:
        ax.set_title(title)

    fig.savefig(ofile)


